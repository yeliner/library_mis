<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/17
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

<style type="text/css">
*{
	font-family:"微软雅黑";
}

body {
	color:#fff;
   margin:0;
   padding:0;}

input,button{
	background-color:#fa9eb1; color:#FFF;
	border:1px #fff solid;
}
#position_info {
	color:#fa9eb1;
}
a:link,a:visited{
	color:#fff;
	
	}
   </style>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript">
 function getByRdId(){
	  var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输借书证号!");
            } 
			else {
                window.location.href = 'addBorrow!getReaderById.action?searchInfo=' + searchInfo;
            }
   }
  
 function getByBkId(){
	  var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入图书序号!");
            } 
			else {
                window.location.href = 'addBorrow!getBookById.action?searchInfo=' + searchInfo;
            }
   }
   
function addBorrow(){
	window.location.href = 'addBorrow!addBorrow.action';
	}
  
</script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<div id="position_info">
  <tr>
    <td height="31" colspan="10"><div class="titlebt"> 基础设置</div></td>
  </tr>
  <tr>
    <td class="left_txt"> 当前位置：基础管理-&gt;借阅管理</td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;借阅</td>
  </tr>
  <tr>
    <td height="30px"></td>
  </tr>
  </div>
  <tr>
    <td align="center" colspan="9"><input type="text" id="searchInfo" style="background-color:#fff; border:1px #fa9eb1 solid;color:#fa9eb1;"/>
      <input type="button" value="根据借书证号查找" id="getByRdId" onClick="getByRdId();"/>
      <input type="button" value="根据图书书号查找" id="getByBkId" onClick="getByBkId();"/>
      <button  onClick="addBorrow();">确定借阅 </button>
      <button  onClick="window.location.href='showBorrow!showBorrow.action'">取消 </button>
  </td>
  </tr>
  <tr><td colspan="9" align="center"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
  <tr>
    <td>读者信息</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="10" align="center" bgcolor="#fa9eb1"  >借书证号</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 读者姓名</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 读者类别</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 证件状态</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 可借书数量</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 可借书天数</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 可续借的次数</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 罚款率（元/天）</td>
          <td height="10" align="center" bgcolor="#fa9eb1"  > 已借书数量</td>
        </tr>
        <tr>
          <td height="10" align="center" bgcolor="#fad7e5"  >${borrowReader.rdId }</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdName}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdType.rdTypeName}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdStatus}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdType.canLendQty}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdType.canLendDay}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdType.canContinueTimes}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdType.punishRate}</td>
          <td height="10" align="center" bgcolor="#fad7e5"  > ${borrowReader.rdBorrowQty}</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td>图书信息</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="10" align="center" bgcolor="#fa9eb1" >图书序号</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 图书编号</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 书名</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 作者</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 出版社</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 出版日期</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > ISBN书号</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 分类号</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 语言</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 页数</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 价格</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 入馆日期</td>
          <td height="10" align="center" bgcolor="#fa9eb1" > 内容简介</td>
          <td height="10" align="center" bgcolor="#fa9eb1" >图书状态</td>
        </tr>
        <tr>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkId } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkCode }</td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkName }</td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkAuthor } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkPress } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkDatePress } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkIsbn } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkCatalog } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkLanguage } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkPages } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkPrice }</td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkDateIn } </td>
          <td height="10" align="center" bgcolor="#fad7e5" 
                        > ${borrowBook.bkBrief } </td>
          <td height="10" align="center" bgcolor="#fad7e5"  id="bkStatus"
                        > ${borrowBook.bkStatus }</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
