<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/16
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>


   <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1 #fff solid;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<div id="position_info">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;图书管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;图书维护</td>
    </tr>
    </div>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr><td><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
                <form name="form1" method="POST" action="updateBook!updateBook.action">
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 图书ID：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkId" type="text" id="bkId"
                                                                             size="30" value="${book.bkId }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 图书编号：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkCode" type="text" id="bkCode"
                                                                             size="30" value="${book.bkCode }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 书名：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkName" type="text" id="bkName"
                                                                             size="30" value="${book.bkName }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 作者：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkAuthor" type="text" id="bkAuthor"
                                                                             size="30" value="${book.bkAuthor }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 出版社：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkPress" type="text" id="bkPress"
                                                                             size="30" value="${book.bkPress }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 出版日期：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkDatePress" type="datetime-local"
                                                                             id="bkDatePress"
                                                                             size="30" value="${book.bkDatePress }"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> ISBN书号：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkIsbn" type="text" id="bkIsbn"
                                                                             size="30" value="${book.bkIsbn }"
                                                                             readonly="readonly"
                                                                             style="background-color: #fa9eb1"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 分类号：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkCatalog" type="text" id="bkCatalog"
                                                                             size="30" value="${book.bkCatalog }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 语言：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><select name="bkLanguage" style="width: 40%">
                            <option value="0">中文</option>
                            <option value="1">英文</option>
                            <option value="2">日文</option>
                            <option value="3">俄文</option>
                            <option value="4">德文</option>
                            <option value="5">法文</option>
                        </select></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 页数：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkPages" type="text" id="bkPages"
                                                                             size="30" value="${book.bkPages }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 价格：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkPrice" type="text" id="bkPrice"
                                                                             size="30" value="${book.bkPrice }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 入馆日期：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkDateIn" type="text" id="bkDateIn"
                                                                             size="30" value="${book.bkDateIn }"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 图书状态：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="bkStatus" type="text" id="bkStatus"
                                                                             size="30" value="${book.bkStatus }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 内容简介：</td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><textarea rows="10" cols="30" style="width: 60%;"
                                                                                name="bkBrief">
                            ${book.bkBrief }
                        </textarea></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>

                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"><input name="back"
                                                                                           type="button" class="button"
                                                                                           id="back"
                                                                                           value="取消"
                                                                                           onClick="window.location.href='showBook!showBook.action'"/>
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td height="32" bgcolor="#fa9eb1" style="color:#fff;"><input name="add"
                                                                 type="submit" class="button" id="add"
                                                                 value="修改" onClick="update();"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                </form>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
