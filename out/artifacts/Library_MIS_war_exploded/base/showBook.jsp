<jsp:useBean id="pageUtil" scope="request" type="qyl.util.PageUtil"/>
<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/13
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<html>
<head>
<style type="text/css">
*{
	font-family:"微软雅黑";
}

body {
	color:#fa9eb1;
   margin:0;
   padding:0;}

input,button{
	background-color:#fa9eb1; color:#FFF;
	border:1 #fff solid;
}

a:link,a:visited{
	color:#fff;
	
	}
   </style>
    <script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
    <script type="text/javascript">


        function show(page) {
            window.location.href = 'showBook!showBook.action?page=' + page;
        }


        function getByName() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入书名!");
            } else {
                window.location.href = 'showBook!getBooksByName.action?searchInfo=' + searchInfo;

            }
        }

        function getByAuthor() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入作者!");
            }
            else {
                window.location.href = 'showBook!getBooksByAuthor.action?searchInfo=' + searchInfo;

            }
        }
        function deleteBook(bkId) {
            if (confirm("您确定要删除图书" + bkId + "吗？")) {
                window.location.href = 'deleteBook!deleteBook.action?delBkId=' + bkId;
            }

        }

    </script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;图书管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;图书显示</td>
    </tr>
    <tr>
        <td align="center" colspan="15"><input type="text" id="searchInfo" style="background-color:#fff; border:1 #fa9eb1 solid;color:#fa9eb1;"/>
            <input type="button" value="根据书名搜索" id="getByName" onClick="getByName();" />
            <input type="button" value="根据作者搜索" id="getByAuthor" onClick="getByAuthor();"/>
            <input type="button" value="获取全部" id="getAll" onClick="window.location.href = 'showBook!showBook.action'"/>
            <button onClick="window.location.href='goAddBook!goAddBook.action'"> 新书入库</button>
    </tr>
    <tr><td align="center" colspan="15"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
    <tr>
        <td height="30px"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;">图书序号</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 图书编号</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 书名</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 作者</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 出版社</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 出版日期</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> ISBN书号</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 分类号</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 语言</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 页数</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 价格</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 入馆日期</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 内容简介</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;">图书状态</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 操作</td>
                </tr>
                <c:forEach var="item" items="${pageUtil.currentList}">
                    <tr>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkId } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkCode }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkName }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkAuthor } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkPress } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkDatePress } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkIsbn } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkCatalog } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkLanguage } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkPages } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkPrice }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkDateIn } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkBrief } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.bkStatus }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        ><a href="goUpdateBook!goUpdateBook.action?bkId=${item.bkId}" >维护</a> <a
                                href="#" target="_self" onClick="deleteBook(${item.bkId});">删除</a></td>
                    </tr>
                </c:forEach>

                <tr>
                    <td height="30" colspan="16" align="center" style="font-family: Arial, Helvetica, sans-serif;
						font-size: 14px;
						line-height: 25px;               
						text-align:right;">共有<strong>${pageUtil.allRecord }</strong>
                        条记录，当前第<strong> ${pageUtil.currentPage }</strong> 页，共 <strong>${pageUtil.allPage }</strong> 页
                        <button
                                onClick="show(1)">首页
                        </button>
                        <button onClick="show(${pageUtil.currentPage-1})">上一页</button>
                        <button
                                onClick="show(${pageUtil.currentPage+1})">下一页
                        </button>
                        <button
                                onClick="show(${pageUtil.allPage})">尾页
                        </button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
