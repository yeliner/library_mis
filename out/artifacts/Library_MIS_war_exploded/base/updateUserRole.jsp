<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/17
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
   
 <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1 #fff solid;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<div id="position_info">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;用户管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;角色修改</td>
    </tr>
    </div>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr><td colspan="3" align="center"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
                <form name="form1" method="POST" action="updateReader!updateReader.action">
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 管理角色：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <select name="rdAdminRoles" style="width: 60%">
                                <option value="0">读者</option>
                                <option value="1">借书证管理</option>
                                <option value="2">图书管理</option>
                                <option value="4">借阅管理</option>
                                <option value="8">系统管理</option>
                            </select>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 证件状态：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdStatus" type="text" id="rdStatus"
                                   size="30" value="${roleUser.rdStatus }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 借书证号：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdId" type="text" id="rdId"
                                   size="30" value="${roleUser.rdId }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>

                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 读者姓名：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdName" type="text" id="rdName"
                                   size="30" value="${roleUser.rdName }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 性别：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdSex" type="text" id="rdSex"
                                   size="30" value="${roleUser.rdSex }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 读者类别：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdType" type="text" id="rdType"
                                                                             size="30"
                                                                             value="${roleUser.rdType.rdType }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                     <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 办证日期：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdDateReg" type="text" id="rdDateReg"
                                   size="30" value="${roleUser.rdDateReg }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 单位名称：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdDept" type="text" id="rdDept"
                                   size="30" value="${roleUser.rdDept }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 电话号码：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdPhone" type="text" id="rdPhone"
                                   size="30" value="${roleUser.rdPhone }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 电子邮箱：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdEmail" type="text" id="rdEmail"
                                   size="30" value="${roleUser.rdEmail }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>

                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 已借书数量：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdBorrowQty" type="text" id="rdBorrowQty"
                                   size="30" value="${roleUser.rdBorrowQty }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 读者密码：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdPwd" type="text" id="rdPwd"
                                   size="30" value="${roleUser.rdPwd }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>

                    <tr>
                        <td height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        ><input name="back"
                                type="button" class="button" id="back"
                                value="取消"
                                onClick="window.location.href='showUser!showUser.action'"/>
                        </td>
                        <td bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="add"
                                                                 type="submit" class="button" id="add"
                                                                 value="修改" onClick="update();"/></td>
                        <td height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                </form>
            </table>
        </td>
    </tr>
</table>
</body>
</html>