<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/19
  Time: 17:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>

   <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1 #fff solid;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <div id="position_info">
    <tr>
        <td height="31">基础设置</td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;读者类别管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;新增类别</td>
    </tr>
    </div>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr><td colspan="3" align="center"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
                <form name="form1" method="POST" id="addForm" action="addReaderType!addReaderType.action">

                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" > 读者类别名称：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="rdTypeName" type="text" id="rdTypeName" size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1"  > 可借书数量：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="canLendQty" type="text" id="canLendQty" size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1"    > 可借书天数：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="canLendDay" type="text" id="canLendDay" size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1"    > 可续借的次数：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="canContinueTimes" type="text" id="canContinueTimes" size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1"    > 罚款率（元/天）：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="punishRate" type="text" id="punishRate" size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1"    > 证书有效期（年）：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="dateValid" type="text" id="dateValid" size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                   

                    <tr>
                        <td height="30" align="right" bgcolor="#fa9eb1"    >
                            <input name="back" type="button" class="button" id="back" value="取消"
                                   onClick="window.location.href='showReaderType!showReaderType.action'"/></td>
                        <td bgcolor="#fa9eb1" >&nbsp;</td>
                        <td height="30" bgcolor="#fa9eb1" >
                            <input name="add" type="submit" class="button" id="add" value="添加" onClick="addBook();"/></td>
                        <td height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                </form>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
