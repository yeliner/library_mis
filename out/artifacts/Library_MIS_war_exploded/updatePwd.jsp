<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/20
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<title>修改密码</title>

<style type="text/css">
*{
	font-family:"微软雅黑";
}

body {
	color:#FFF;
	background-color:#c9ebec;
   margin:0;}
#id {
	width:100%;
	border:none;
	margin:0px auto;
	height:80px;
	font-size: 28px;
	font-weight: bolder;	
   padding-left:3%;
}
#id1 {
	width:100%;
	border:1px #90d7d7 solid;
}
</style>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript">
        function updatePwd() {
          /*  var oldRdPwd = $("#oldRdPwd").val();*/
			var oldRdPwd= document.getElementsByTagName('span')[0].innerHTML;
            var newRdPwd = $("#newRdPwd").val();
            var sureRdPwd = $("#sureRdPwd").val();
            if (newRdPwd === "" || sureRdPwd === "") {
                alert("请输入完整信息!");
                return;
            }
            if (newRdPwd === oldRdPwd) {
                alert("新密码与原密码相同!!");
                return;
            }
            if (newRdPwd !== sureRdPwd) {
                alert("新密码与确认密码不一致!!");
                return;
            }
            $.ajax({
                type: "post",
                url: "user!updateUserPwd.action",
                dataType: "text",
                data:{newRdPwd:newRdPwd},
                success: function (result) {
                    if (result === "success") {
                        window.location.href = "main.jsp";
                    }
                    else {
                        alert(result);
                    }
                }
            });
        }
    </script>
</head>
<body>
<div id="id"><br/>长江大学图书馆信息管理系统-密码修改</div>
<div id="id1" >
   <form name="myform" action=""  method="post">
    <table width="100%"  height="500" border="0"  cellspacing="0" cellpadding="0" >
      <tr>
        <td colspan="2" height="35" style="background-color:#fad7e5; " ><b>密码信息</b></td>
      </tr>
       <tr>
        <td colspan="2" height="35" style="color:#fa9eb1">用户名: &nbsp;&nbsp;${reader.rdName }</td>          
        </tr>
        <tr>
          <td colspan="2" height="35" style="color:#fa9eb1">原密码: &nbsp;&nbsp;<span>${reader.rdPwd }</span></td>  
        </tr>
          <tr>
        <td colspan="2" height="35"  style="background-color:#fad7e5" ><b>修改密码</b></td>
      </tr>
      <tr>
        <td  height="35" width="135" align="right"><span>请输入新密码 &nbsp;&nbsp; </span></td>
          <td height="35"  align="left"> &nbsp;&nbsp;   <input  type="password" size="40" name="newRdPwd" id="newRdPwd"></td>
      </tr>
      <tr>
        <td  height="35" width="135" align="right"><span>再次输入密码 &nbsp;&nbsp; </span></td>
          <td height="35"  align="left">  &nbsp;&nbsp;  <input  type="password" size="40" name="sureRdPwd" id="sureRdPwd"></td>
      </tr>
       <tr>
          <td  height="35" align="right" >
           <input name="login" type="button" class="button" id="login"  value="修 改" onClick="updatePwd();" style="background-color:#fad7e5; color:#FFF; width:60px; height:35px;">&nbsp;&nbsp;</td>
          <td  height="35"    align="left"> &nbsp;&nbsp;<input
                                            name="cs" type="button" class="button" id="cs"
                                            value="取消" onClick="window.history.go(-1)" style="background-color:#fad7e5; color:#FFF; width:60px; height:35px;"></td>
        </tr>
    </table>
    </form>
</div>
</body>
</html>
