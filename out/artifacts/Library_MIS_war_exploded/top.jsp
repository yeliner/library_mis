<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/15
  Time: 11:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="UTF-8">
    <title>长江大学图书馆信息管理系统</title>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script language=JavaScript>
        function logout() {
            if (confirm("您确定要退出控制面板吗？"))
                $session.clear();
            top.location.href = "out.jsp";
            return false;
        }

        function updatePwd() {
            window.parent.location.href = "updatePwd.jsp"
        }
    </script>
    <script type="text/javascript">
        $.ajax({
            type: "post",
            url: "login!getSessionUser.action",
            dataType: "text",
            success: function (result) {
                if (result === "nologin") {
                    window.parent.location.href = "out.jsp";
                }
                else {
                    document.getElementsByTagName('b')[0].innerHTML = result;
                }
            }
        });
    </script>
    <style>
        body {
            font: 20px Arial, Helvetica, sans-serif;
            color: #000;
            background-color: #EEF2FB;
            margin: 0px;
        }
    </style>
    <base target="main">
</head>
<body leftmargin="0" topmargin="0">
<table width="100%" height="64" border="0" cellpadding="0"
       cellspacing="0" class="topbg">
    <tr>
        <td width="80%" height="38" class="admin_txt" align="center">用户：<b></b>您好,感谢登陆使用！</td>
        <td width="20%" align="rigth"><a href="#" target="_self"
                                         onClick="logout();">安全退出</a>
            <a href="#" target="_self"
               onClick="updatePwd();">密码修改</a>

        </td>
    </tr>
</table>
</body>
</html>
