<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/19
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>长江大学图书馆信息管理系统</title>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript">
        $(window).load(function () {

            //8-系统管理
            if ($("#role").text() === "0") { //0-读者
                $("#reader_li").show();
                $("#book_li").hide();
                $("#borrow_li").show();
                $("#user_li").hide();
                $("#type_li").hide();
            }else if ($("#role").text() === "1") { //1-借书证管理员
                $("#reader_li").show();
                $("#book_li").hide();
                $("#borrow_li").hide();
                $("#user_li").hide();
                $("#type_li").hide();
            }else if ($("#role").text() === "2") {  //2-图书管理员
                $("#reader_li").hide();
                $("#book_li").show();
                $("#borrow_li").hide();
                $("#user_li").hide();
                $("#type_li").hide();
            }else if ($("#role").text() === "4") {   //4-借阅管理
                $("#reader_li").hide();
                $("#book_li").hide();
                $("#borrow_li").show();
                $("#user_li").hide();
                $("#type_li").hide();
            }else {
                $("#reader_li").show();
                $("#book_li").show();
                $("#borrow_li").show();
                $("#user_li").show();
                $("#type_li").show();
            }
        });
    </script>
</head>
<style>

    body {
        font: 16px Arial, Helvetica, sans-serif;
        color: #000;
        background-color: #EEF2FB;
        margin: 0px;
    }

    #container {
        width: 150px;
    }

    H1 {
        font-size: 18px;
        margin: 0px;
        width: 150px;
        cursor: pointer;
        height: 30px;
        line-height: 20px;
    }

    H1 a {
        display: block;
        width: 150px;
        color: #000;
        height: 30px;
        text-decoration: none;
        moz-outline-style: none;
        background-color: #EEF2EB;
        line-height: 30px;
        text-align: center;
        margin: 0px;
        padding: 0px;
    }

    .content {
        width: 150px;
        height: 26px;

    }

    .MM ul {
        list-style-type: none;
        margin: 0px;
        padding: 0px;
        display: block;
    }

    .MM li {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 26px;
        color: #333333;
        list-style-type: none;
        display: block;
        text-decoration: none;
        height: 26px;
        width: 150px;
        padding-left: 0px;
    }

    .MM {
        width: 150px;
        margin: 0px;
        padding: 0px;
        left: 0px;
        top: 0px;
        right: 0px;
        bottom: 0px;
        clip: rect(0px, 0px, 0px, 0px);
    }

    .MM a:link {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 26px;
        color: #333333;
        background-color: #EEE8EB;
        height: 26px;
        width: 150px;
        display: block;
        text-align: center;
        margin: 0px;
        padding: 0px;
        overflow: hidden;
        text-decoration: none;
    }

    .MM a:visited {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 26px;
        color: #333333;
        background-color: #EEE8EB;
        display: block;
        text-align: center;
        margin: 0px;
        padding: 0px;
        height: 26px;
        width: 150px;
        text-decoration: none;
    }

    .MM a:active {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 26px;
        color: #333333;
        background-color: #EEE8EB;
        height: 26px;
        width: 150px;
        display: block;
        text-align: center;
        margin: 0px;
        padding: 0px;
        overflow: hidden;
        text-decoration: none;
    }

    .MM a:hover {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 26px;
        font-weight: bold;
        color: #006600;
        background-color: #EEE2FB;
        text-align: center;
        display: block;
        margin: 0px;
        padding: 0px;
        height: 26px;
        width: 150px;
        text-decoration: none;
    }
</style>
<body>
<table width="100%" height="280" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="3" align="center"><label id="role" hidden="hidden">${reader.rdAdminRoles}</label></td>
    </tr>

    <tr>
        <td width="150" valign="top">
            <div id="container">
                <h1 class="type"><a>基础管理</a></h1>
                <div class="content">
                    <ul class="MM" id="main_ul">
                        <li id="reader_li"><a href="showReader!showReader.action" target="main">借书证管理</a></li>
                        <li id="book_li"><a href="showBook!showBook.action" target="main">图书管理</a></li>
                        <li id="borrow_li"><a href="showBorrow!showBorrow.action" target="main">借阅管理</a></li>
                        <li id="user_li"><a href="showUser!showUser.action" target="main">用户管理</a></li>
                        <li id="type_li"><a href="showReaderType!showReaderType.action" target="main">读者类别管理</a></li>
                    </ul>
                </div>
            </div>
            <script type="text/javascript">
                var contents = document.getElementsByClassName('content');
                var toggles = document.getElementsByClassName('type');

                var myAccordion = new fx.Accordion(
                    toggles, contents, {opacity: true, duration: 200}
                );
                myAccordion.showThisHideOpen(contents[0]);
            </script>
        </td>
    </tr>
</table>
</body>
</html>