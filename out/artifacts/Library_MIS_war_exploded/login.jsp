<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/12
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
    <head>
    <title>login</title>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript">
        function boolFrom() {

            var rdId = $("#rdId").val();
            var rdPwd = $("#rdPwd").val();
            if (rdId == "" || rdPwd == "") {
                alert("请输入完整信息!");
                return;
            }
            $.ajax({
                type: "post",
                url: "login!login.action",
                dataType: "text",
                data:{rdId:rdId,rdPwd:rdPwd},
                success: function (result) {
                    if (result == "success") {
                        window.location.href = "main.jsp";
                    }
                    else {
                        alert(result);
                    }
                }
            });
        }
    </script>
    </head>

    <body >
    <form name="myform" action=""  method="post">
      <table width="50%" height="300" border="0" align="center" cellpadding="0"
       cellspacing="0">
        <tr>
          <td width="50%"  height="38" align="center" colspan="2"><span>登陆长江大学图书馆信息管理系统</span></td>
        </tr>
        <tr>
          <td height="35" align="right"><span>用户编号 &nbsp;&nbsp; </span></td>
          <td height="35"  align="left"><input  name="rdId"  value="" size="20" id="rdId"></td>
        </tr>
        <tr>
          <td height="35"  align="right"><span> 用户密码 &nbsp;&nbsp; </span></td>
          <td height="35"   align="left"><input type="password" size="20" name="rdPwd" id="rdPwd"></td>
        </tr>
        <tr>
          <td height="35" align="right"> <input name="login"type="button" class="button" id="login"  value="登 陆" onClick="boolFrom();">&nbsp;&nbsp;</td>
          <td height="35" align="left" > &nbsp;&nbsp; <input  name="reset" type="reset" class="button" id="reset"  value="重 置"></td>
        </tr>
      </table>
    </form>
</body>
</html>
