<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/17
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
    <head>
    <title>login</title>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript">
        function updatePwd() {
            var oldRdPwd = $("#oldRdPwd").val();
            var newRdPwd = $("#newRdPwd").val();
            var sureRdPwd = $("#sureRdPwd").val();
            if (newRdPwd == "" || sureRdPwd == "") {
                alert("请输入完整信息!");
                return;
            }
            if (newRdPwd == oldRdPwd) {
                alert("新密码与原密码相同!!");
                return;
            }
            if (newRdPwd != sureRdPwd) {
                alert("新密码与确认密码不一致!!");
                return;
            }
            $.ajax({
                type: "post",
                url: "user!updateUserPwd.action",
                dataType: "text",
                data:{newRdPwd:newRdPwd},
                success: function (result) {
                    if (result == "success") {
                        window.location.href = "main.jsp";
                    }
                    else {
                        alert(result);
                    }
                }
            });
        }
    </script>
    </head>

    <body>
    <form name="myform" action=""  method="post">
      <table width="50%" height="300" border="0" align="center" cellpadding="0"
       cellspacing="0">
        <tr>
          <td  height="38" align="center" colspan="2"><span> 长江大学图书馆信息管理系统-密码修改</span></td>
        </tr>
        <tr>
          <td height="35"   align="right"><span>用户名 &nbsp;&nbsp; </span></td>
          <td height="35"  align="left"><input name="rdName"
                                                                                                         
                                                                                                         value="${reader.rdName }"
                                                                                                         size="20"
                                                                                                         id="rdName"
                                                                                                         style="background-color: #f2f2f2"
                                                                                                         readonly="readonly"/></td>
        </tr>
        <tr>
          <td width="40%" height="35" align="right"><span> 原密码 &nbsp;&nbsp; </span></td>
          <td height="35"   align="left"><input
                                            type="text" size="20"
                                            name="oldRdPwd" id="oldRdPwd" value="${reader.rdPwd }"
                                            style="background-color: #f2f2f2" readonly="readonly"></td>
        </tr>
        <tr>
          <td  height="35" align="right"><span>新密码 &nbsp;&nbsp; </span></td>
          <td height="35"  align="left"><input
                                           type="password" size="20"
                                            name="newRdPwd" id="newRdPwd"></td>
        </tr>
        <tr>
          <td height="35" align="right"><span>确认密码 &nbsp;&nbsp; </span></td>
          <td height="35"    align="left"><input
                                          type="password" size="20"
                                            name="sureRdPwd" id="sureRdPwd"></td>
        </tr>
        <tr>
          <td  height="35" align="right" > <input name="login"
                                                                       type="button" class="button" id="login"
                                                                       value="修 改" onClick="updatePwd();">&nbsp;&nbsp;</td>
          <td  height="35"    align="left"> &nbsp;&nbsp;<input
                                            name="cs" type="button" class="button" id="cs"
                                            value="取消" onClick="window.history.go(-1)"></td>
        </tr>
      </table>
    </form>
</body>
</html>
