<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/12
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>长江大学图书馆信息管理系统</title>


    <style type="text/css">
        * {
            font-family: "微软雅黑";
        }

        #big {
            width: 100%;
            height: 100%;
        }

        #top {
            padding: 0px;
            width: 100%;
            height: 100px;
        }

        #topFrame {
            width: 100%;
            height: 100%;
        }

        #center {
            padding: 0px;
            height: 800px;
            width: 100%;
            border: 1px;
        }

        #left {
            padding: 0px;
            width: 15%;
            height: 100%;
            float: left;
        }

        #leftFrame {
            width: 100%;
            height: 100%;
        }

        #right {
            padding: 0px;
            width: 85%;
            height: 100%;
            float: left;
        }

        #main {
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
<div id="big" align="center">
    <div id="top">
        <iframe id="topFrame" name="topFrame" frameborder="0" scrolling="no" src="top2.jsp"></iframe>
    </div>
    <div id="center">
        <div id="left">
            <iframe id="leftFrame" name="leftFrame" frameborder="0" src="left2.jsp"></iframe>
        </div>
        <div id="right">
            <iframe id="main" name="main" frameborder="0" src="right.jsp"></iframe>
        </div>
    </div>
</div>
</body>
</html>
