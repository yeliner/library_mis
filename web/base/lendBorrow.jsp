<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/19
  Time: 13:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>


   <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1 #fff solid;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <div id="position_info">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;借阅管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;归还图书</td>
    </tr>
    </div>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><label style="color: red"><strong>${msgInfo}</strong></label></td>
                </tr>
                <form name="form1" method="POST" action="updateBorrow!lendBorrow.action">
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 借书顺序号：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" >
                            <input name="borrowId" type="text" id="borrowId"
                                   size="30" value="${lendBorrow.borrowId }"
                                   style="background-color: #fff"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>

                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 读者序号：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="lendRdId" type="text" id="lendRdId"  size="30" value="${lendBorrow.tbReaderByRdId.rdId }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                     <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 图书序号：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="lendBkId" type="text" id="lendBkId"  size="30" value="${lendBorrow.tbBookByBkId.bkId }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                     <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 续借次数：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldContinueTimes" type="text" id="ldContinueTimes"  size="30" value="${lendBorrow.ldContinueTimes }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                    
                   <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 借书日期：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldDateOut" type="text" id="ldDateOut"  size="30" value="${lendBorrow.ldDateOut }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                     <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 应还日期：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldDateRetPlan" type="text" id="ldDateRetPlan"  size="30" value="${lendBorrow.ldDateRetPlan }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                      <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 实际还书日期：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldDateRetAct" type="text" id="ldDateRetAct"  size="30" value="${lendBorrow.ldDateRetAct }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                      <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 超期天数：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldOverDay" type="text" id="ldOverDay"  size="30" value="${lendBorrow.ldOverDay }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                      <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 超期金额：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldOverMoney" type="text" id="ldOverMoney"  size="30" value="${lendBorrow.ldOverMoney }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 罚款金额：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="ldPunishMoney" type="text" id="ldPunishMoney"  size="30" value="${lendBorrow.ldPunishMoney }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                     <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" 
                        > 借书操作员：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="operatorRet" type="text" id="operatorRet"  size="30" value="${lendBorrow.operatorRet }" style="background-color: #fff"
                                   readonly="readonly"/>
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td height="30" align="right" bgcolor="#fa9eb1" 
                        ><input name="back"
                                type="button" class="button" id="back"
                                value="取消"
                                onClick="window.location.href='showBorrow!showBorrow.action'"/></td>
                        <td bgcolor="#fa9eb1" >&nbsp;</td>
                        <td height="30" bgcolor="#fa9eb1" ><input name="add"
                                                                 type="submit" class="button" id="add"
                                                                 value="还书" onClick="update();"/></td>
                        <td height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                </form>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
