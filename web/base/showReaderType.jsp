<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/19
  Time: 17:37
  To change this template use File | Settings | File Templates.
--%>
<jsp:useBean id="pageUtil" scope="request" type="qyl.util.PageUtil"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<html>
<head>



<style type="text/css">
*{
	font-family:"微软雅黑";
}

body {
	color:#fa9eb1;
   margin:0;
   padding:0;}

input,button{
	background-color:#fa9eb1; color:#FFF;
	border:1 #fff solid;
}

a:link,a:visited{
	color:#fff;
	
	}
   </style>
    <script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
    <script type="text/javascript">


        function show(page) {
            window.location.href = 'showReaderType!showReaderType.action?page=' + page;
        }


        function getByName() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入类别名!");
            } else {
                window.location.href = 'showReaderType!getReaderTypesByName.action?searchInfo=' + searchInfo;

            }
        }

        function deleteReaderType(rdType) {
            if (confirm("您确定要删除类别" + rdType + "吗？")) {
                window.location.href = 'deleteReaderType!deleteReaderType.action?delRdType=' + rdType;
            }

        }

    </script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" >
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;读者类别管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;类别显示</td>
    </tr>
    <tr>
        <td align="center" colspan="15"><input type="text" id="searchInfo" style="background-color:#fff; border:1 #fa9eb1 solid;color:#fa9eb1;"/>
            <input type="button" value="根据类别名搜索" id="getByName" onClick="getByName();"/>
            <input type="button" value="获取全部" id="getAll" onClick="window.location.href = 'showReaderType!showReaderType.action'"/>
            <button onClick="window.location.href='goAddReaderType!goAddReaderType.action'">新增类别</button>
    </tr>
    <tr><td align="center" colspan="15"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
    <tr>
        <td height="30px"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;">读者类别</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 读者类别名称</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 可借书数量</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 可借书天数</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 可续借的次数</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 罚款率（元/天）</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 证书有效期</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 操作</td>
                </tr>
                <c:forEach var="item" items="${pageUtil.currentList}">
                    <tr>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.rdType } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.rdTypeName }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.canLendQty }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.canLendDay } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.canContinueTimes } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.punishRate } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.dateValid } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        ><a href="goUpdateReaderType!goUpdateReaderType.action?rdType=${item.rdType}">修改</a> <a
                                href="#" target="_self" onClick="deleteReaderType(${item.rdType});">删除</a></td>
                    </tr>
                </c:forEach>

                <tr>
                    <td height="30" colspan="16" align="center" style="font-family: Arial, Helvetica, sans-serif;
						font-size: 14px;
						line-height: 25px;               
						text-align:right;">共有<strong>${pageUtil.allRecord }</strong>
                        条记录，当前第<strong> ${pageUtil.currentPage }</strong> 页，共 <strong>${pageUtil.allPage }</strong> 页
                        <button
                                onClick="show(1)">首页
                        </button>
                        <button onClick="show(${pageUtil.currentPage-1})">上一页</button>
                        <button
                                onClick="show(${pageUtil.currentPage+1})">下一页
                        </button>
                        <button
                                onClick="show(${pageUtil.allPage})">尾页
                        </button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
