<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/17
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="pageUtil" scope="request" type="qyl.util.PageUtil"/>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<html>
<head>


<style type="text/css">
*{
	font-family:"微软雅黑";
}

body {
	color:#fa9eb1;
   margin:0;
   padding:0;}

input,button{
	background-color:#fa9eb1; color:#FFF;
	border:1 #fff solid;
}

a:link,a:visited{
	color:#fff;
	
	}
   </style>
<script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
<script type="text/javascript">


        function show(page) {
            window.location.href = 'showUser!showUser.action?page=' + page;
        }
  		function getById() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入借书证号!");
            } else {
                window.location.href = 'showUser!getUsersById.action?searchInfo=' + searchInfo;

            }
        }

        function getByName() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入读者姓名!");
            } else {
                window.location.href = 'showUser!getUsersByName.action?searchInfo=' + searchInfo;

            }
        }

        function getByType() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入读者类别!");
            } else {
                window.location.href = 'showUser!getUsersByType.action?searchInfo=' + searchInfo;

            }
        }

        function getByDept() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入单位名称!");
            }
            else {
                window.location.href = 'showUser!getUsersByDept.action?searchInfo=' + searchInfo;

            }
        }

        function updateRole(rdId) {
            window.location.href = 'goUpdateReader!goUpdateUserRole.action?roleId=' + rdId;
        }
    </script>
</head>
<body>
<table width="100%" border="0" cellpadding="0">
  <tr>
    <td height="31"><div class="titlebt"> 基础设置</div></td>
  </tr>
  <tr>
    <td class="left_txt"> 当前位置：基础管理-&gt;用户管理</td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;用户显示</td>
  </tr>
  <tr>
    <td align="center" colspan="10"><input type="text" id="searchInfo"  style="background-color:#fff; border:1 #fa9eb1 solid;color:#fa9eb1;"/>
      <input type="button" value="根据借书证号查找" id="getById" onClick="getById();"/>
      <input type="button" value="根据姓名查找" id="getByName" onClick="getByName();"/>
      <input type="button" value="根据类别查找" id="getByType" onClick="getByType();"/>
      <input type="button" value="根据单位查找" id="getByDept" onClick="getByDept();"/>
      <input type="button" value="查找所有" id="getAll"
                   onClick="window.location.href = 'showUser!showUser.action'"/></td>
  </tr>

    <tr><td colspan="10" align="center"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
  <tr>
    <td height="30px"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" >借书证号</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 读者姓名</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 性别</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 读者类别</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 单位名称</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 电话号码</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 电子邮箱</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 办证日期</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 证件状态</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 已借书数量</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 读者密码</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 管理角色</td>
          <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;" > 操作</td>
        </tr>
        <c:forEach var="item" items="${pageUtil.currentList}">
          <tr>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdId } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdName }</td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdSex }</td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdType.rdTypeName } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdDept } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdPhone } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdEmail } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdDateReg } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdStatus } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdBorrowQty } </td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdPwd }</td>
            <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                            > ${item.rdAdminRoles } </td
                        >
            <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"
                            ><a  href="#" target="_self" onClick="updateRole(${item.rdId});">角色修改</a></td>
          </tr>
        </c:forEach>
        <tr>
          <td height="30" colspan="16" align="center" style="font-family: Arial, Helvetica, sans-serif;
						font-size: 14px;
						line-height: 25px;               
						text-align:right;">共有<strong>${pageUtil.allRecord }</strong> 条记录，当前第<strong> ${pageUtil.currentPage }</strong> 页，共 <strong>${pageUtil.allPage }</strong> 页
            <button onClick="show(1)">首页</button>
            <button onClick="show(${pageUtil.currentPage-1})">上一页</button>
            <button onClick="show(${pageUtil.currentPage+1})">下一页</button>
            <button onClick="show(${pageUtil.allPage})">尾页</button></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>