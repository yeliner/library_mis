<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/16
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>

   <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1 #fff solid;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <div id="position_info">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;借书证管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;办理图书证</td>
    </tr>
    <tr>
        <td height="30px"></td>
    </tr>
    </div>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr><td colspan="3" align="center"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
                <form name="form1" method="POST" action="addReader!addReader.action">
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" >读者姓名：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="rdName" type="text" id="title1"
                                                                             size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" 
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" > 性别：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input type="radio" name="rdSex" value="男"
                                                                             checked>
                            男
                            <input type="radio" name="rdSex" value="女">
                            女
                        </td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" > 读者类别：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><select name="rdTypeId" style="width: 43%">
                            <c:forEach items="${list}" var="item">
                                <option value="${item.rdType}">${item.rdTypeName }</option>
                            </c:forEach>
                        </select></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" > 单位名称：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="rdDept" type="text" id="title4"
                                                                             size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" >电话号码：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="rdPhone" type="text" id="title5"
                                                                             size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" > 电子邮箱：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="rdEmail" type="text" id="title6"
                                                                             size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" > 读者密码：</td>
                        <td width="3%" bgcolor="#fa9eb1" >&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" ><input name="rdPwd" type="text" id="title7"
                                                                             size="30"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                    <tr>
                        <td height="30" align="right" bgcolor="#fa9eb1" ><input name="back" type="button" class="button"
                                                                               id="back" value="取消"
                                                                               onClick="window.location.href='showReader!showReader.action'"/>
                        </td>
                        <td bgcolor="#fa9eb1" >&nbsp;</td>
                        <td height="30" bgcolor="#fa9eb1" ><input name="add" type="submit" class="button" id="add"
                                                                 value="添加"
                                                                 onClick="add();"/></td>
                        <td height="30" bgcolor="#fa9eb1"  class="left_txt"></td>
                    </tr>
                </form>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
