<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/17
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<html>
<head>

<style type="text/css">
*{
	font-family:"微软雅黑";
}

body {
	color:#fa9eb1;
   margin:0;
   padding:0;}

input,button{
	background-color:#fa9eb1; color:#FFF;
	border:1 #fff solid;
}

a:link,a:visited{
	color:#fff;
	
	}
   </style>
    <script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
    <script type="text/javascript">


        function show(page) {
            window.location.href = 'showBorrow!showBorrow.action?page=' + page;
        }

        function getByRdId() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入借书证号!");
            } else {
                window.location.href = 'showBorrow!getBorrowsByRdId.action?searchInfo=' + searchInfo;
            }
        }

        function getByBkId() {
            var searchInfo = document.getElementById('searchInfo').value;
            if (searchInfo === "") {
                alert("请输入图书序号!");
            } else {
                window.location.href = 'showBorrow!getBorrowsByBkId.action?searchInfo=' + searchInfo;
            }
        }
		
        function lendBorrow(lsHasReturn, borrowId) {
            if (lsHasReturn) {
                alert("该书已还!!");
            } else {
                window.location.href = 'goUpdateBorrow!goLendBorrow.action?lendBorrowId=' + borrowId;
            }
        }
		  function continueBorrow(lsHasReturn, borrowId) {
            if (lsHasReturn) {
                alert("该书已还!!");
            } else {
                window.location.href = 'updateBorrow!continueBorrow.action?continueBorrowId=' + borrowId;
            }
        }
    </script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;借阅管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;借阅信息显示</td>
    </tr>
    <tr>
        <td align="center" colspan="14"><input type="text" id="searchInfo" style="background-color:#fff; border:1px #fa9eb1 solid;color:#fa9eb1;"/>
            <input type="button" value="根据借书证号查找" id="getByRdId" onClick="getByRdId();"/>
            <input type="button" value="根据图书序号查找" id="getByBkId" onClick="getByBkId();"/>
            <input type="button" value="查找所有" id="getAll"
                   onClick="window.location.href = 'showBorrow!showBorrow.action'"/>
            <button onClick="window.location.href='goAddBorrow!goAddBorrow.action'"> 借书</button>
            </td>

    </tr>
     <tr><td align="center" colspan="14"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
    <tr>
        <td height="30px"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;">借书顺序号</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 读者姓名</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 图书编号</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 续借次数</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 借书日期</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 应还日期</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 实际还书日期</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 超期天数</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 超期金额</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 罚款金额</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 是否已还书</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 借书操作员</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 还书操作员</td>
                    <td height="10" align="center" bgcolor="#fa9eb1" style="color:#fff;"> 操作</td>
                </tr>
                <jsp:useBean id="pageUtil" scope="request" type="qyl.util.PageUtil"/>
                <c:forEach var="item" items="${pageUtil.currentList}">
                    <tr>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.borrowId } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.tbReaderByRdId.rdName }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.tbBookByBkId.bkCode }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldContinueTimes }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldDateOut } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldDateRetPlan } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldDateRetAct } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldOverDay } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldOverMoney } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.ldPunishMoney } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.lsHasReturn } </td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.operatorRet }</td>
                        <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        > ${item.operatorLend } </td>
                          <td height="10" align="center" bgcolor="#fad7e5" style="color:#fff;"
                        >
                            <a href="#" target="_self"   onClick="lendBorrow(${item.lsHasReturn},${item.borrowId});">还书</a>
                            <a style="margin-left: 10px" href="#" target="_self"
                               onClick="continueBorrow(${item.lsHasReturn},${item.borrowId});">续借</a>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td height="30" colspan="16" align="center" style="font-family: Arial, Helvetica, sans-serif;
						font-size: 14px;
						line-height: 25px;               
						text-align:right;">共有<strong>${pageUtil.allRecord }</strong>
                        条记录，当前第<strong> ${pageUtil.currentPage }</strong> 页，共 <strong>${pageUtil.allPage }</strong> 页
                        <button onClick="show(1)">首页</button>
                        <button onClick="show(${pageUtil.currentPage-1})">上一页</button>
                        <button onClick="show(${pageUtil.currentPage+1})">下一页</button>
                        <button onClick="show(${pageUtil.allPage})">尾页</button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
