<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/16
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
 <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1px #fff solid;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<div id="position_info">
    <tr>
        <td height="31">
            <div class="titlebt"> 基础设置</div>
        </td>
    </tr>
    <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;借书证管理</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;借书证挂失/注销/补办</td>
    </tr>
    </div>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr><td colspan="3" align="center"><label style="color: red"><strong>${msgInfo}</strong></label></td></tr>
                <form name="form1" method="POST" action="updateReader!updateReader.action">

                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 证件状态：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><select name="rdStatus" style="width: 40%">
                            <option value="有效">有效</option>
                            <option value="注销">注销</option>
                            <option value="挂失">挂失</option>
                        </select></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 借书证号：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdId" type="text" id="rdId"
                                                                             size="30" value="${statusReader.rdId }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 读者姓名：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdName" type="text" id="rdName"
                                                                             size="30" value="${statusReader.rdName }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 性别：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdSex" type="text" id="rdSex"
                                                                             size="30" value="${statusReader.rdSex }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 读者类别：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdTypeId" type="text" id="title2"
                                                                             size="30"
                                                                             value="${statusReader.rdType.rdType }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 单位名称：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fa9eb1;"><input name="rdDept" type="text" id="rdDept"
                                                                             size="30" value="${statusReader.rdDept }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 电话号码：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdPhone" type="text" id="rdPhone"
                                                                             size="30" value="${statusReader.rdPhone }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 电子邮箱：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdEmail" type="text" id="rdEmail"
                                                                             size="30" value="${statusReader.rdEmail }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                     <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 办证日期：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;">
                            <input name="rdDateReg" type="text" id="rdDateReg"
                                   size="30" value="${statusReader.rdDateReg }"
                                   style="background-color: #fa9eb1"
                                   readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 已借书数量：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdBorrowQty" type="text"
                                                                             id="rdBorrowQty"
                                                                             size="30"
                                                                             value="${statusReader.rdBorrowQty }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 读者密码：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdPwd" type="text" id="rdPwd"
                                                                             size="30" value="${statusReader.rdPwd }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        > 管理角色：
                        </td>
                        <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdAdminRoles" type="text"
                                                                             id="rdAdminRoles"
                                                                             size="30"
                                                                             value="${statusReader.rdAdminRoles }"
                                                                             style="background-color: #fa9eb1"
                                                                             readonly="readonly"/></td>
                        <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;"
                            class="left_txt"></td>
                    </tr>
                    <tr>
                        <td height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"
                        ><input name="back"
                                type="button" class="button" id="back"
                                value="取消"
                                onClick="window.location.href='showReader!showReader.action'"/></td>
                        <td bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                        <td height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="add"
                                                                 type="submit" class="button" id="add"
                                                                 value="修改" onClick="updateReader();"/></td>
                        <td height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
                    </tr>
                </form>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
