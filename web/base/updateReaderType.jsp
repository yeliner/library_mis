<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/19
  Time: 17:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
    <head>
    
 <style type="text/css">
* {
	font-family:"微软雅黑";
}
body {
	color:#fff;
	margin:0;
	padding:0;
}
#position_info {
	color:#fa9eb1;
}
#back,#add{
	background-color:#FFF; 
	color:#fa9eb1;
	border:1 #fff solid;
}
</style>
    </head>

    <body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <div id="position_info">
      <tr>
        <td height="31"><div class="titlebt"> 基础设置</div></td>
      </tr>
      <tr>
        <td class="left_txt"> 当前位置：基础管理-&gt;读者类别管理</td>
      </tr>
      <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;类别修改</td>
      </tr>
      </div>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td><label style="color: red"><strong>${msgInfo}</strong></label></td>
          </tr>
            <form name="form1" method="POST" action="updateReaderType!updateReaderType.action">
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 读者类别：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdType" type="text" id="rdType"
                                                                             size="30" value="${readerType.rdType }"
                                                                             style="background-color: #fff"
                                                                             readonly="readonly"/></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 读者类别名称：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="rdTypeName" type="text" id="rdTypeName"
                                                                             size="30" value="${readerType.rdTypeName }"
                                                                         /></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 可借书数量：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="canLendQty" type="text" id="canLendQty"
                                                                             size="30" value="${readerType.canLendQty }"/></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 可借书天数：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="canLendDay" type="text" id="canLendDay"
                                                                             size="30" value="${readerType.canLendDay }"/></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 可续借的次数：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="canContinueTimes" type="text" id="canContinueTimes"
                                                                             size="30" value="${readerType.canContinueTimes }"/></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 罚款率（元/天）：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="PunishRate" type="text"
                                                                             id="PunishRate"
                                                                             size="30" value="${readerType.punishRate }"/></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"> 证书有效期：</td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td width="32%" height="30" bgcolor="#fa9eb1" style="color:#fff;"><input name="dateValid" type="text" id="dateValid"
                                                                             size="30" value="${readerType.dateValid }"
                                                                             /></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
            <tr>
                <td width="20%" height="30" align="right" bgcolor="#fa9eb1" style="color:#fff;"><input name="back"
                                                                                           type="button" class="button"
                                                                                           id="back"
                                                                                           value="取消"
                                                                                           onClick="window.location.href='showReaderType!showReaderType.action'"/></td>
                <td width="3%" bgcolor="#fa9eb1" style="color:#fff;">&nbsp;</td>
                <td height="32" bgcolor="#fa9eb1" style="color:#fff;"><input name="add"
                                                                 type="submit" class="button" id="add"
                                                                 value="修改" onClick="update();"/></td>
                <td width="45%" height="30" bgcolor="#fa9eb1" style="color:#fff;" class="left_txt"></td>
              </tr>
          </form>
          </table></td>
      </tr>
    </table>
</body>
</html>
