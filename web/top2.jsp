<%--
  Created by IntelliJ IDEA.
  User: Yeliner
  Date: 2018/12/20
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
    <style type="text/css">
*{
	font-family:"微软雅黑";
}
#big {
	color:#fff;
	height: 84px;
	width: 100%;
	background-color:#c9ebec;
}
#big1 {
	height: 84px;
	width: 100%;
}
#logo {
	margin-left:40px;
	float: left;
	height: 84px;
	width: 84px;
}
#info {
	font-size:24px;
	float: left;
	height: 84px;
	width: 50%;
	text-align: center;
	padding-top: 3%;
}
#s {
	height: 84px;
	width: 170px;
	float: right;
	margin-right:10px;
}
#left, #right {
	padding-top: 13%;
	text-align: center;
	width: 85px;
	height: 60px;
	float: left;
}
a:link{
	color:#fff;
	
	}
</style>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript">
        $.ajax({
            type: "post",
            url: "login!getSessionUser.action",
            dataType: "text",
            success: function (result) {
                if (result === "nologin") {
                    window.parent.location.href = "out.jsp";
                }
                else {
                    document.getElementsByTagName('b')[0].innerHTML = result;
                }
            }
        });

        function logout() {
            if (confirm("您确定要退出控制面板吗？")) {
                top.location.href = "out.jsp";
            }
        }

        function updatePwd() {
            window.parent.location.href = "updatePwd.jsp"
        }
    </script>
    </head>
    <body id="big">
    <div id="big1">
      <div id="logo"><img src="img/head3.png" width="84" height="84"/></div>
      <div id="info">用户：<b></b>您好,感谢登陆使用！</div>
      <div id="s">
        <div id="left"><a href="#" target="_self" onClick="updatePwd();" >密码修改</a></div>
        <div id="right"><a href="#" target="_self" onClick="logout();">安全退出</a></div>
        <div style="clear:both"></div>
      </div>
    </div>
</body>
</html>
