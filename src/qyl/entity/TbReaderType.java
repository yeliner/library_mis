package qyl.entity;


import javax.persistence.*;

@Entity
@Table(name = "TB_ReaderType", schema = "dbo", catalog = "Library")
public class TbReaderType {
    private int rdType;
    private String rdTypeName;
    private Integer canLendQty;
    private Integer canLendDay;
    private Integer canContinueTimes;
    private Double punishRate;
    private int dateValid;

    @Id
    @Column(name = "rdType", nullable = false)
    public int getRdType() {
        return rdType;
    }

    public void setRdType(int rdType) {
        this.rdType = rdType;
    }

    @Basic
    @Column(name = "rdTypeName", nullable = false, length = 20)
    public String getRdTypeName() {
        return rdTypeName;
    }

    public void setRdTypeName(String rdTypeName) {
        this.rdTypeName = rdTypeName;
    }

    @Basic
    @Column(name = "canLendQty", nullable = false)
    public Integer getCanLendQty() {
        return canLendQty;
    }

    public void setCanLendQty(Integer canLendQty) {
        this.canLendQty = canLendQty;
    }

    @Basic
    @Column(name = "canLendDay", nullable = false)
    public Integer getCanLendDay() {
        return canLendDay;
    }

    public void setCanLendDay(Integer canLendDay) {
        this.canLendDay = canLendDay;
    }

    @Basic
    @Column(name = "canContinueTimes", nullable = false)
    public Integer getCanContinueTimes() {
        return canContinueTimes;
    }

    public void setCanContinueTimes(Integer canContinueTimes) {
        this.canContinueTimes = canContinueTimes;
    }

    @Basic
    @Column(name = "punishRate", nullable = false, precision = 0)
    public Double getPunishRate() {
        return punishRate;
    }

    public void setPunishRate(Double punishRate) {
        this.punishRate = punishRate;
    }

    @Basic
    @Column(name = "dateValid", nullable = false)
    public int getDateValid() {
        return dateValid;
    }

    public void setDateValid(int dateValid) {
        this.dateValid = dateValid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbReaderType that = (TbReaderType) o;

        if (rdType != that.rdType) return false;
        if (rdTypeName != null ? !rdTypeName.equals(that.rdTypeName) : that.rdTypeName != null) return false;
        if (canLendQty != null ? !canLendQty.equals(that.canLendQty) : that.canLendQty != null) return false;
        if (canLendDay != null ? !canLendDay.equals(that.canLendDay) : that.canLendDay != null) return false;
        if (canContinueTimes != null ? !canContinueTimes.equals(that.canContinueTimes) : that.canContinueTimes != null)
            return false;
        if (punishRate != null ? !punishRate.equals(that.punishRate) : that.punishRate != null) return false;
        if (dateValid != that.dateValid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rdType;
        result = 31 * result + (rdTypeName != null ? rdTypeName.hashCode() : 0);
        result = 31 * result + (canLendQty != null ? canLendQty.hashCode() : 0);
        result = 31 * result + (canLendDay != null ? canLendDay.hashCode() : 0);
        result = 31 * result + (canContinueTimes != null ? canContinueTimes.hashCode() : 0);
        result = 31 * result + (punishRate != null ? punishRate.hashCode() : 0);
        result = dateValid;
        return result;
    }

}
