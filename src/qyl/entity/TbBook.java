package qyl.entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TB_Book", schema = "dbo", catalog = "Library")
public class TbBook {
    private int bkId;
    private String bkCode;
    private String bkName;
    private String bkAuthor;
    private String bkPress;
    private Date bkDatePress;
    private String bkIsbn;
    private String bkCatalog;
    private int bkLanguage;
    private Integer bkPages;
    private Float bkPrice;
    private Date bkDateIn;
    private String bkBrief;
    private String bkStatus;

    @Id
    @Column(name = "bkID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getBkId() {
        return bkId;
    }

    public void setBkId(int bkId) {
        this.bkId = bkId;
    }

    @Basic
    @Column(name = "bkCode", nullable = false, length = 20)
    public String getBkCode() {
        return bkCode;
    }

    public void setBkCode(String bkCode) {
        this.bkCode = bkCode;
    }

    @Basic
    @Column(name = "bkName", nullable = false, length = 50)
    public String getBkName() {
        return bkName;
    }

    public void setBkName(String bkName) {
        this.bkName = bkName;
    }

    @Basic
    @Column(name = "bkAuthor", nullable = false, length = 30)
    public String getBkAuthor() {
        return bkAuthor;
    }

    public void setBkAuthor(String bkAuthor) {
        this.bkAuthor = bkAuthor;
    }

    @Basic
    @Column(name = "bkPress", nullable = true, length = 50)
    public String getBkPress() {
        return bkPress;
    }

    public void setBkPress(String bkPress) {
        this.bkPress = bkPress;
    }

    @Basic
    @Column(name = "bkDatePress", nullable = true)
    public Date getBkDatePress() {
        return bkDatePress;
    }

    public void setBkDatePress(Date bkDatePress) {
        this.bkDatePress = bkDatePress;
    }

    @Basic
    @Column(name = "bkISBN", nullable = false, length = 15)
    public String getBkIsbn() {
        return bkIsbn;
    }

    public void setBkIsbn(String bkIsbn) {
        this.bkIsbn = bkIsbn;
    }

    @Basic
    @Column(name = "bkCatalog", nullable = true, length = 30)
    public String getBkCatalog() {
        return bkCatalog;
    }

    public void setBkCatalog(String bkCatalog) {
        this.bkCatalog = bkCatalog;
    }

    @Basic
    @Column(name = "bkLanguage", nullable = true)
    public int getBkLanguage() {
        return bkLanguage;
    }

    public void setBkLanguage(int bkLanguage) {
        this.bkLanguage = bkLanguage;
    }

    @Basic
    @Column(name = "bkPages", nullable = true)
    public Integer getBkPages() {
        return bkPages;
    }

    public void setBkPages(Integer bkPages) {
        this.bkPages = bkPages;
    }

    @Basic
    @Column(name = "bkPrice", nullable = true)
    public Float getBkPrice() {
        return bkPrice;
    }

    public void setBkPrice(Float bkPrice) {
        this.bkPrice = bkPrice;
    }

    @Basic
    @Column(name = "bkDateIn", nullable = true)
    public Date getBkDateIn() {
        return bkDateIn;
    }

    public void setBkDateIn(Date bkDateIn) {
        this.bkDateIn = bkDateIn;
    }

    @Basic
    @Column(name = "bkBrief", nullable = true, length = -1)
    public String getBkBrief() {
        return bkBrief;
    }

    public void setBkBrief(String bkBrief) {
        this.bkBrief = bkBrief;
    }

    @Basic
    @Column(name = "bkStatus", nullable = false, length = 2)
    public String getBkStatus() {
        return bkStatus;
    }

    public void setBkStatus(String bkStatus) {
        this.bkStatus = bkStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbBook book = (TbBook) o;

        if (bkId != book.bkId) return false;
        if (bkCode != null ? !bkCode.equals(book.bkCode) : book.bkCode != null) return false;
        if (bkName != null ? !bkName.equals(book.bkName) : book.bkName != null) return false;
        if (bkAuthor != null ? !bkAuthor.equals(book.bkAuthor) : book.bkAuthor != null) return false;
        if (bkPress != null ? !bkPress.equals(book.bkPress) : book.bkPress != null) return false;
        if (bkDatePress != null ? !bkDatePress.equals(book.bkDatePress) : book.bkDatePress != null) return false;
        if (bkIsbn != null ? !bkIsbn.equals(book.bkIsbn) : book.bkIsbn != null) return false;
        if (bkCatalog != null ? !bkCatalog.equals(book.bkCatalog) : book.bkCatalog != null) return false;
        if (bkLanguage !=book.bkLanguage)  return false;
        if (bkPages != null ? !bkPages.equals(book.bkPages) : book.bkPages != null) return false;
        if (bkPrice != null ? !bkPrice.equals(book.bkPrice) : book.bkPrice != null) return false;
        if (bkDateIn != null ? !bkDateIn.equals(book.bkDateIn) : book.bkDateIn != null) return false;
        if (bkBrief != null ? !bkBrief.equals(book.bkBrief) : book.bkBrief != null) return false;
        if (bkStatus != null ? !bkStatus.equals(book.bkStatus) : book.bkStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bkId;
        result = 31 * result + (bkCode != null ? bkCode.hashCode() : 0);
        result = 31 * result + (bkName != null ? bkName.hashCode() : 0);
        result = 31 * result + (bkAuthor != null ? bkAuthor.hashCode() : 0);
        result = 31 * result + (bkPress != null ? bkPress.hashCode() : 0);
        result = 31 * result + (bkDatePress != null ? bkDatePress.hashCode() : 0);
        result = 31 * result + (bkIsbn != null ? bkIsbn.hashCode() : 0);
        result = 31 * result + (bkCatalog != null ? bkCatalog.hashCode() : 0);
        result = bkLanguage ;
        result = 31 * result + (bkPages != null ? bkPages.hashCode() : 0);
        result = 31 * result + (bkPrice != null ? bkPrice.hashCode() : 0);
        result = 31 * result + (bkDateIn != null ? bkDateIn.hashCode() : 0);
        result = 31 * result + (bkBrief != null ? bkBrief.hashCode() : 0);
        result = 31 * result + (bkStatus != null ? bkStatus.hashCode() : 0);
        return result;
    }
}
