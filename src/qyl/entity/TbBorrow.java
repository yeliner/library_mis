package qyl.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TB_Borrow", schema = "dbo", catalog = "Library")
public class TbBorrow {
    private int borrowId;
    private Integer ldContinueTimes;
    private Date ldDateOut;
    private Date ldDateRetPlan;
    private Date ldDateRetAct;
    private Integer ldOverDay;
    private Double ldOverMoney;
    private Double ldPunishMoney;
    private Boolean lsHasReturn;
    private String operatorLend;
    private String operatorRet;
    private TbBook tbBookByBkId;
    private TbReader tbReaderByRdId;

    @Id
    @Column(name = "borrowID", nullable = false, precision = 0)
    public int getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(int borrowId) {
        this.borrowId = borrowId;
    }

    @ManyToOne
    @JoinColumn(name = "bkID", referencedColumnName = "bkID", nullable = false)
    public TbBook getTbBookByBkId() {
        return tbBookByBkId;
    }

    public void setTbBookByBkId(TbBook tbBookByBkId) {
        this.tbBookByBkId = tbBookByBkId;
    }

    @ManyToOne
    @JoinColumn(name = "rdID", referencedColumnName = "rdID", nullable = false)
    public TbReader getTbReaderByRdId() {
        return tbReaderByRdId;
    }

    public void setTbReaderByRdId(TbReader tbReaderByRdId) {
        this.tbReaderByRdId = tbReaderByRdId;
    }
    @Basic
    @Column(name = "ldDateOut", nullable = false)
    public Date getLdDateOut() {
        return ldDateOut;
    }

    public void setLdDateOut(Date ldDateOut) {
        this.ldDateOut = ldDateOut;
    }
    @Basic
    @Column(name = "ldContinueTimes", nullable = false)
    public Integer getLdContinueTimes() {
        return ldContinueTimes;
    }

    public void setLdContinueTimes(Integer ldContinueTimes) {
        this.ldContinueTimes = ldContinueTimes;
    }
    @Basic
    @Column(name = "ldDateRetPlan", nullable = false)
    public Date getLdDateRetPlan() {
        return ldDateRetPlan;
    }

    public void setLdDateRetPlan(Date ldDateRetPlan) {
        this.ldDateRetPlan = ldDateRetPlan;
    }

    @Basic
    @Column(name = "ldDateRetAct", nullable = true)
    public Date getLdDateRetAct() {
        return ldDateRetAct;
    }

    public void setLdDateRetAct(Date ldDateRetAct) {
        this.ldDateRetAct = ldDateRetAct;
    }

    @Basic
    @Column(name = "ldOverDay", nullable = true)
    public Integer getLdOverDay() {
        return ldOverDay;
    }

    public void setLdOverDay(Integer ldOverDay) {
        this.ldOverDay = ldOverDay;
    }

    @Basic
    @Column(name = "ldOverMoney", nullable = true)
    public Double getLdOverMoney() {
        return ldOverMoney;
    }

    public void setLdOverMoney(Double ldOverMoney) {
        this.ldOverMoney = ldOverMoney;
    }

    @Basic
    @Column(name = "ldPunishMoney", nullable = true)
    public Double getLdPunishMoney() {
        return ldPunishMoney;
    }

    public void setLdPunishMoney(Double ldPunishMoney) {
        this.ldPunishMoney = ldPunishMoney;
    }

    @Basic
    @Column(name = "lsHasReturn", nullable = false)
    public Boolean getLsHasReturn() {
        return lsHasReturn;
    }

    public void setLsHasReturn(Boolean lsHasReturn) {
        this.lsHasReturn = lsHasReturn;
    }

    @Basic
    @Column(name = "OperatorLend", nullable = true, length = 20)
    public String getOperatorLend() {
        return operatorLend;
    }

    public void setOperatorLend(String operatorLend) {
        this.operatorLend = operatorLend;
    }

    @Basic
    @Column(name = "OperatorRet", nullable = false, length = 20)
    public String getOperatorRet() {
        return operatorRet;
    }

    public void setOperatorRet(String operatorRet) {
        this.operatorRet = operatorRet;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbBorrow tbBorrow = (TbBorrow) o;

        if (borrowId != tbBorrow.borrowId) return false;
        if (ldContinueTimes != null ? !ldContinueTimes.equals(tbBorrow.ldContinueTimes) : tbBorrow.ldContinueTimes != null)
            return false;
        if (ldDateOut != null ? !ldDateOut.equals(tbBorrow.ldDateOut) : tbBorrow.ldDateOut != null) return false;
        if (ldDateRetPlan != null ? !ldDateRetPlan.equals(tbBorrow.ldDateRetPlan) : tbBorrow.ldDateRetPlan != null)
            return false;
        if (ldDateRetAct != null ? !ldDateRetAct.equals(tbBorrow.ldDateRetAct) : tbBorrow.ldDateRetAct != null)
            return false;
        if (ldOverDay != null ? !ldOverDay.equals(tbBorrow.ldOverDay) : tbBorrow.ldOverDay != null) return false;
        if (ldOverMoney != null ? !ldOverMoney.equals(tbBorrow.ldOverMoney) : tbBorrow.ldOverMoney != null)
            return false;
        if (ldPunishMoney != null ? !ldPunishMoney.equals(tbBorrow.ldPunishMoney) : tbBorrow.ldPunishMoney != null)
            return false;
        if (lsHasReturn != null ? !lsHasReturn.equals(tbBorrow.lsHasReturn) : tbBorrow.lsHasReturn != null)
            return false;
        if (operatorLend != null ? !operatorLend.equals(tbBorrow.operatorLend) : tbBorrow.operatorLend != null)
            return false;
        if (operatorRet != null ? !operatorRet.equals(tbBorrow.operatorRet) : tbBorrow.operatorRet != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = borrowId;
        result = 31 * result + (ldContinueTimes != null ? ldContinueTimes.hashCode() : 0);
        result = 31 * result + (ldDateOut != null ? ldDateOut.hashCode() : 0);
        result = 31 * result + (ldDateRetPlan != null ? ldDateRetPlan.hashCode() : 0);
        result = 31 * result + (ldDateRetAct != null ? ldDateRetAct.hashCode() : 0);
        result = 31 * result + (ldOverDay != null ? ldOverDay.hashCode() : 0);
        result = 31 * result + (ldOverMoney != null ? ldOverMoney.hashCode() : 0);
        result = 31 * result + (ldPunishMoney != null ? ldPunishMoney.hashCode() : 0);
        result = 31 * result + (lsHasReturn != null ? lsHasReturn.hashCode() : 0);
        result = 31 * result + (operatorLend != null ? operatorLend.hashCode() : 0);
        result = 31 * result + (operatorRet != null ? operatorRet.hashCode() : 0);
        return result;
    }


}
