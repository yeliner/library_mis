package qyl.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TB_Reader", schema = "dbo", catalog = "Library")
public class TbReader {
    private int rdId;
    private String rdName;
    private String rdSex;
    private String rdDept;
    private String rdPhone;
    private String rdEmail;
    private Date rdDateReg;
    private String rdStatus;
    private int rdBorrowQty;
    private String rdPwd;
    private int rdAdminRoles;
    private TbReaderType rdType;

    @Id
    @Column(name = "rdID", nullable = false)
    public int getRdId() {
        return rdId;
    }

    public void setRdId(int rdId) {
        this.rdId = rdId;
    }

    @ManyToOne
    @JoinColumn(name = "rdType",referencedColumnName = "rdType", nullable = false)
    public TbReaderType getRdType() {
        return rdType;
    }

    public void setRdType(TbReaderType rdType) {
        this.rdType = rdType;
    }

    @Basic
    @Column(name = "rdName", nullable = false, length = 20)
    public String getRdName() {
        return rdName;
    }

    public void setRdName(String rdName) {
        this.rdName = rdName;
    }

    @Basic
    @Column(name = "rdSex", nullable = false, length = 1)
    public String getRdSex() {
        return rdSex;
    }

    public void setRdSex(String rdSex) {
        this.rdSex = rdSex;
    }

    @Basic
    @Column(name = "rdDept", nullable = true, length = 20)
    public String getRdDept() {
        return rdDept;
    }

    public void setRdDept(String rdDept) {
        this.rdDept = rdDept;
    }

    @Basic
    @Column(name = "rdPhone", nullable = true, length = 25)
    public String getRdPhone() {
        return rdPhone;
    }

    public void setRdPhone(String rdPhone) {
        this.rdPhone = rdPhone;
    }

    @Basic
    @Column(name = "rdEmail", nullable = true, length = 25)
    public String getRdEmail() {
        return rdEmail;
    }

    public void setRdEmail(String rdEmail) {
        this.rdEmail = rdEmail;
    }

    @Basic
    @Column(name = "rdDateReg", nullable = true)
    public Date getRdDateReg() {
        return rdDateReg;
    }

    public void setRdDateReg(Date rdDateReg) {
        this.rdDateReg = rdDateReg;
    }

    @Basic
    @Column(name = "rdStatus", nullable = false, length = 2)
    public String getRdStatus() {
        return rdStatus;
    }

    public void setRdStatus(String rdStatus) {
        this.rdStatus = rdStatus;
    }

    @Basic
    @Column(name = "rdBorrowQty", nullable = false)
    public int getRdBorrowQty() {
        return rdBorrowQty;
    }

    public void setRdBorrowQty(int rdBorrowQty) {
        this.rdBorrowQty = rdBorrowQty;
    }

    @Basic
    @Column(name = "rdPwd", nullable = false, length = 20)
    public String getRdPwd() {
        return rdPwd;
    }

    public void setRdPwd(String rdPwd) {
        this.rdPwd = rdPwd;
    }

    @Basic
    @Column(name = "rdAdminRoles", nullable = false)
    public int getRdAdminRoles() {
        return rdAdminRoles;
    }

    public void setRdAdminRoles(int rdAdminRoles) {
        this.rdAdminRoles = rdAdminRoles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbReader reader = (TbReader) o;

        if (rdId != reader.rdId) return false;
        if (rdName != null ? !rdName.equals(reader.rdName) : reader.rdName != null) return false;
        if (rdSex != null ? !rdSex.equals(reader.rdSex) : reader.rdSex != null) return false;
        if (rdDept != null ? !rdDept.equals(reader.rdDept) : reader.rdDept != null) return false;
        if (rdPhone != null ? !rdPhone.equals(reader.rdPhone) : reader.rdPhone != null) return false;
        if (rdEmail != null ? !rdEmail.equals(reader.rdEmail) : reader.rdEmail != null) return false;
        if (rdDateReg != null ? !rdDateReg.equals(reader.rdDateReg) : reader.rdDateReg != null) return false;
        if (rdStatus != null ? !rdStatus.equals(reader.rdStatus) : reader.rdStatus != null) return false;
        if (rdBorrowQty != reader.rdBorrowQty) return false;
        if (rdPwd != null ? !rdPwd.equals(reader.rdPwd) : reader.rdPwd != null) return false;
        if (rdAdminRoles != reader.rdAdminRoles) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rdId;
        result = 31 * result + (rdName != null ? rdName.hashCode() : 0);
        result = 31 * result + (rdSex != null ? rdSex.hashCode() : 0);
        result = 31 * result + (rdDept != null ? rdDept.hashCode() : 0);
        result = 31 * result + (rdPhone != null ? rdPhone.hashCode() : 0);
        result = 31 * result + (rdEmail != null ? rdEmail.hashCode() : 0);
        result = 31 * result + (rdDateReg != null ? rdDateReg.hashCode() : 0);
        result = 31 * result + (rdStatus != null ? rdStatus.hashCode() : 0);
        result = rdBorrowQty;
        result = 31 * result + (rdPwd != null ? rdPwd.hashCode() : 0);
        result = rdAdminRoles;
        return result;
    }
}
