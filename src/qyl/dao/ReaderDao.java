package qyl.dao;

import qyl.dao.help.HelperDao;
import qyl.entity.TbReader;

import java.util.Date;
import java.util.List;
import java.util.Objects;


public class ReaderDao {
    private HelperDao hd;

    public HelperDao getHd() {
        return hd;
    }

    public void setHd(HelperDao hd) {
        this.hd = hd;
    }

    public TbReader getTbReaderById(int rdId) {
        // hql语句
        String hql = "from TbReader where rdId=?";
        return (TbReader) hd.queryById(rdId, hql);
    }


    //通过姓名查找读者
    public List queryByName(String rdName) {
        String hql = "from TbReader where rdName like ?";
        Object[] params = {"%" + rdName + "%"};
        return hd.queryByParams(hql, params);
    }

    //通过类别查找读者
    public List queryByType(String rdType) {
        String hql = "from TbReader where rdType.rdTypeName=?";
        Object[] params = {rdType};
        return hd.queryByParams(hql, params);
    }

    //通过班级查找读者
    public List queryByDept(String rdDept) {
        String hql = "from TbReader where rdDept like ?";
        Object[] params = {"%" + rdDept + "%"};
        return hd.queryByParams(hql, params);
    }

    //添加
    public boolean addTbReader(TbReader reader) {
        reader.setRdId(getNum());//读者id自动生成
        if(Objects.equals(reader.getRdPwd(), "")){
            reader.setRdPwd("123");//默认为123
        }

        return hd.addObject(reader);
    }

    //删除
    public boolean deleteTbReader(int rdId) {
        return hd.deleteObject(this.getTbReaderById(rdId));
    }

    //修改
    public boolean updateTbReader(TbReader reader) {
        return hd.updateObject(reader);

    }

    public List queryByPage(int pageSize, int currentPage) {
        String hql = "from TbReader as m order by m.rdId ";
        return hd.queryByPage(pageSize, currentPage, hql);
    }

    public int countAllNum() {
        String hql = "select count(*) from TbReader";
        return hd.countAllNum(hql);

    }

    private int getNum() {
        String hql = "select max(rdId) from TbReader";
        return hd.getNum(hql);
    }

}
