package qyl.dao;

import qyl.dao.help.HelperDao;
import qyl.entity.TbReaderType;

import java.util.List;

public class ReaderTypeDao {
    private HelperDao hd;

    public HelperDao getHd() {
        return hd;
    }

    public void setHd(HelperDao hd) {
        this.hd = hd;
    }

    public List queryAll() {
        String hql = "from TbReaderType";
        return hd.queryByParams(hql, null);
    }

    public TbReaderType queryByRdType(int rdType) {
        String hql = "from TbReaderType where rdType=?";
       return (TbReaderType) hd.queryById(rdType,hql);
    }

    //通过名获取
    public List queryByName(String name) {
        String hql = "from TbReaderType where rdTypeName like ?";
        Object[] params = {"%" + name + "%"};
        return hd.queryByParams(hql, params);
    }

    public TbReaderType queryByName1(String name) {
        String hql = "from TbReaderType where rdTypeName=?";
        Object[] params = {name};
        List list=hd.queryByParams(hql, params);
        if(list.size()>0){
            return (TbReaderType)list .get(0);
        }
        return null;
    }
    //添加
    public boolean addTbReaderType(TbReaderType TbReaderType) {
        TbReaderType.setRdType(getNum());//自动生成
        return hd.addObject(TbReaderType);

    }
    //修改
    public boolean updateTbReaderType(TbReaderType TbReaderType) {
        return hd.updateObject(TbReaderType);
    }

    //删除
    public boolean deleteTbReaderType(int rdType) {
        return hd.deleteObject(this.queryByRdType(rdType));
    }

    public List queryByPage(int pageSize, int currentPage) {
        String hql = "from TbReaderType as m order by m.rdType ";
        return hd.queryByPage(pageSize, currentPage, hql);
    }

    public int countAllNum() {
        String hql = "select count(*) from TbReaderType";
        return hd.countAllNum(hql);

    }
    private int getNum() {
        String hql = "select max(rdType) from TbReaderType";
        return hd.getNum(hql);
    }
}

