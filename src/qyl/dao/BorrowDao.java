package qyl.dao;

import qyl.dao.help.HelperDao;
import qyl.entity.TbBorrow;

import java.util.List;

public class BorrowDao {
    private HelperDao hd;

    public HelperDao getHd() {
        return hd;
    }

    public void setHd(HelperDao hd) {
        this.hd = hd;
    }

    public TbBorrow getTbBorrowById(int borrowId) {
        // hql语句
        String hql = "from TbBorrow where borrowId=?";
        return (TbBorrow) hd.queryById(borrowId, hql);
    }


    //添加
    public boolean addTbBorrow(TbBorrow borrow) {
        borrow.setBorrowId(getNum());//编号自动生成
        String hql = "update TbReader set rdBorrowQty=rdBorrowQty+1 where rdId=?";
        Object[] params = {borrow.getTbReaderByRdId().getRdId()};
        if (!hd.updateByHQL(hql, params)) {
            return false;
        } else {
            hql = " update TbBook set bkStatus='借出' where bkId=?";
            Object[] params2 = {borrow.getTbBookByBkId().getBkId()};
            if (!hd.updateByHQL(hql, params2)) {
                hql = "update TbReader set rdBorrowQty=rdBorrowQty-1 where rdId=?";
                Object[] params3 = {borrow.getTbReaderByRdId().getRdId()};
                hd.updateByHQL(hql, params3);
                return false;
            } else {
                if (!hd.addObject(borrow)) {
                    hql = "update TbReader set rdBorrowQty=rdBorrowQty-1 where rdId=?";
                    Object[] params4 = {borrow.getTbReaderByRdId().getRdId()};
                    hd.updateByHQL(hql, params4);
                    hql = " update TbBook set bkStatus='在馆' where bkId=?";
                    Object[] params5 = {borrow.getTbBookByBkId().getBkId()};
                    hd.updateByHQL(hql, params5);
                    return false;
                }
            }
        }
        return true;
    }

    //删除
    public boolean deleteTbBorrow(int rdId) {
        return hd.deleteObject(this.getTbBorrowById(rdId));
    }

    //还书
    public boolean updateLendBorrow(TbBorrow borrow) {
        String hql = "update TbReader set rdBorrowQty=rdBorrowQty-1 where rdId=?";
        Object[] params = {borrow.getTbReaderByRdId().getRdId()};
        if (!hd.updateByHQL(hql, params)) {
            return false;
        } else {
            hql = " update TbBook set bkStatus='在馆' where bkId=?";
            Object[] params2 = {borrow.getTbBookByBkId().getBkId()};
            if (!hd.updateByHQL(hql, params2)) {
                hql = "update TbReader set rdBorrowQty=rdBorrowQty+1 where rdId=?";
                Object[] params3 = {borrow.getTbReaderByRdId().getRdId()};
                hd.updateByHQL(hql, params3);
                return false;
            } else {
                if (!hd.updateObject(borrow)) {
                    hql = "update TbReader set rdBorrowQty=rdBorrowQty+1 where rdId=?";
                    Object[] params4 = {borrow.getTbReaderByRdId().getRdId()};
                    hd.updateByHQL(hql, params4);
                    hql = " update TbBook set bkStatus='借出' where bkId=?";
                    Object[] params5 = {borrow.getTbBookByBkId().getBkId()};
                    hd.updateByHQL(hql, params5);
                    return false;
                }else {
                    return true;
                }
            }
        }

    }
    public boolean updateContinueBorrow(TbBorrow borrow) {
                return hd.updateObject(borrow);
    }
    public List queryByPage(int pageSize, int currentPage) {
        String hql = "from TbBorrow as m order by m.borrowId ";
        return hd.queryByPage(pageSize, currentPage, hql);
    }

    public int countAllNum() {
        String hql = "select count(*) from TbBorrow";
        return hd.countAllNum(hql);

    }

    private int getNum() {
        String hql = "select max(borrowId) from TbBorrow";
        return hd.getNum(hql);
    }

    public List getTbBorrowByRdId(int id) {
        // hql语句
        String hql = "from TbBorrow where tbReaderByRdId.rdId=?";
        Object[] params = {id};
        return hd.queryByParams(hql, params);
    }

    public List getTbBorrowByBkId(int id) {
        // hql语句
        String hql = "from TbBorrow where tbBookByBkId.bkId=?";
        Object[] params = {id};
        return hd.queryByParams(hql, params);
    }



}
