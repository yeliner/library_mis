package qyl.dao;

import qyl.dao.help.HelperDao;
import qyl.entity.TbBook;

import java.util.Date;
import java.util.List;

public class BookDao {
    private HelperDao hd;

    public HelperDao getHd() {
        return hd;
    }

    public void setHd(HelperDao hd) {
        this.hd = hd;
    }

    public TbBook getTbBookById(int bkId) {
        // hql语句
        String hql = "from TbBook where bkId=?";
        return (TbBook) hd.queryById(bkId, hql);
    }

    //通过书名获取图书
    public List queryByName(String bkName) {
        String hql = "from TbBook where bkName like ?";
        Object[] params = {"%" + bkName + "%"};
        return hd.queryByParams(hql, params);
    }

    public List queryByAuthor(String bkAuthor) {
        String hql = "from TbBook where bkAuthor like ?";
        Object[] params = {"%" + bkAuthor + "%"};
        return hd.queryByParams(hql, params);
    }
    //添加
    public boolean addTbBook(TbBook tbBook) {
        tbBook.setBkCode(this.getBkCode());//图书编目生成
        return hd.addObject(tbBook);

    }

    //修改
    public boolean updateTbBook(TbBook tbBook) {
        return hd.updateObject(tbBook);

    }

    public String getBkCode() {
        //生成图书编号
        String hql = "select max(bkId) from TbBook";
        return "BOOK" + hd.getNum(hql);
    }

    //删除
    public boolean deleteTbBook(int bkId) {
        return hd.deleteObject(getTbBookById(bkId));
    }

    public List queryByPage(int pageSize, int currentPage) {
        String hql = "from TbBook as m order by m.bkId ";
        return hd.queryByPage(pageSize, currentPage, hql);
    }

    public int countAllNum() {
        String hql = "select count(*) from TbBook";
        return hd.countAllNum(hql);

    }


}
