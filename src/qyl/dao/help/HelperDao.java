package qyl.dao.help;

import org.hibernate.*;

import java.util.List;

public class HelperDao {
    // 这个类就完全可以实现对数据库的基本操作
    //此类适用于hibernate4以上版本
    //hibernate4用hibernateSupport很容易引发一系列的问题,如果想避免的话,可以用此类
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private static Session session;
    private static Transaction tr;

    //根据id查找
    public Object queryById(int Id, String hql) {
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery(hql);
            query.setInteger(0, Id);
            List list = query.list();
            if (list.size() > 0) {
                return list.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    //根据参数条件查找
    public List queryByParams(String sql, Object[] params) {
        List list;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery(sql);
            if (params != null) {
                if (params.length > 0) {
                    for (int i = 0; i < params.length; i++) {
                        query.setParameter(i, params[i]);
                    }
                }
            }
            list = query.list();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    //分页操作
    public List queryByPage(int pageSize, int currentPage, String sql) {

        List list;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery(sql);
            query.setFirstResult(pageSize * (currentPage - 1));
            query.setMaxResults(pageSize);
            list = query.list();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public int countAllNum(String sql) {
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery(sql);
            List paramList = query.list();
            long num = (Long) paramList.get(0);
            return (int) num;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return 0;
    }

    public int getNum(String hql) {
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery(hql);
            List paramList = query.list();
            if (paramList.size() > 0) {
                return ((Integer) paramList.get(0)) + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return 1;
    }

    //添加操作
    public boolean addObject(Object object) {
        if (session != null) {
            session = null;
        }
        try {
            session = sessionFactory.openSession();
            tr = session.beginTransaction();
            session.save(object);
            tr.commit();
            return true;
        } catch (HibernateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            tr.rollback();
        } finally {
            session.close();
        }
        return false;
    }

    //删除操作
    public boolean deleteObject(Object object) {
        try {
            session = sessionFactory.openSession();
            tr = session.beginTransaction();
            session.delete(object);
            tr.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            tr.rollback();
        } finally {
            session.close();
        }
        return false;
    }

    //修改操作
    public boolean updateObject(Object object) {
        try {
            session = sessionFactory.openSession();
            tr = session.beginTransaction();
            session.update(object);
            tr.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            tr.rollback();
        } finally {
            session.close();
        }
        return false;
    }

    //根据参数修改操作
    public boolean updateByHQL(String hql, Object[] params) {
        try {
            session = sessionFactory.openSession();
            tr = session.beginTransaction();
            Query query = session.createQuery(hql);
            if (params != null) {
                if (params.length > 0) {
                    for (int i = 0; i < params.length; i++) {
                        query.setParameter(i, params[i]);
                    }
                }
            }
            query.executeUpdate();
            tr.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            tr.rollback();
        } finally {
            session.close();
        }
        return false;
    }
}
