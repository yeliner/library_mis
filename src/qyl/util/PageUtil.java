package qyl.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.jstl.sql.Result;

public class PageUtil {
	private int allRecord;//总记录数;
	private int allPage;//总页数;
	private int currentPage;//当前页;
	private List currentList;//每页显示的记录集合
	private int pageSize=4;//每页显示多少条记录
	private int first;//每页第一条记录的索引
	private int last;//每页最后一条记录的索引
	private Result result;
	//处理的方法
	public void count(String scp){
		currentList = new ArrayList();
		if(allRecord==0){
			allPage=0;
			currentPage=0;
		}else{
			allPage=allRecord%pageSize==0?allRecord/pageSize:allRecord/pageSize+1;
			int cp;
			try {
			cp=Integer.parseInt(scp);
			} catch (Exception e) {
				cp=1;
			}
			if(cp<1){
				currentPage=1;
			}else if(cp>allPage){
				currentPage=allPage;
			}else{
				currentPage=cp;
			}
			first=(currentPage-1)*pageSize;
			if(currentPage==allPage){
				last=allRecord-1;
			}else{
				last=currentPage*pageSize-1;
			}
		}
	}
	public int getAllRecord() {
		return allRecord;
	}
	public void setAllRecord(int allRecord) {
		this.allRecord = allRecord;
	}
	public int getAllPage() {
		return allPage;
	}
	public void setAllPage(int allPage) {
		this.allPage = allPage;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List getCurrentList() {
		return currentList;
	}
	public void setCurrentList(List currentList) {
		this.currentList = currentList;
	}
	public int getFirst() {
		return first;
	}
	public void setFirst(int first) {
		this.first = first;
	}
	public int getLast() {
		return last;
	}
	public void setLast(int last) {
		this.last = last;
	}
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}

}
