package qyl.services;

import qyl.dao.ReaderTypeDao;
import qyl.entity.TbReader;
import qyl.entity.TbReaderType;

import java.util.List;

public class ReaderTypeServices {
    private ReaderTypeDao rtd;

    public ReaderTypeDao getRtd() {
        return rtd;
    }

    public void setRtd(ReaderTypeDao rtd) {
        this.rtd = rtd;
    }

    public List getAllReaderTypes() {
        return rtd.queryAll();
    }

    public TbReaderType getByRdType(int rdType) {
        return this.rtd.queryByRdType(rdType);
    }
    public List queryAll() {
        return rtd.queryAll();
    }

    public TbReaderType queryByRdType(int rdType) {
       return rtd.queryByRdType(rdType);
    }

    //通过名获取
    public List queryByName(String name) {
     return rtd.queryByName(name);
    }

    //添加
    public boolean addTbReaderType(TbReaderType tbReaderType) {
        return rtd.addTbReaderType(tbReaderType);

    }
    //修改
    public boolean updateTbReaderType(TbReaderType TbReaderType) {
        return rtd.updateTbReaderType(TbReaderType);
    }

    //删除
    public boolean deleteTbReaderType(int rdType) {
        return rtd.deleteTbReaderType(rdType);
    }

    public List queryByPage(int pageSize, int currentPage) {
        return rtd.queryByPage(pageSize, currentPage);
    }

    public int getCountAllNum() {
        return rtd.countAllNum();
    }

    public TbReaderType queryByName1(String rdTypeName) {
        return rtd.queryByName1(rdTypeName);
    }
}
