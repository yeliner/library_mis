package qyl.services;

import qyl.dao.ReaderDao;
import qyl.entity.TbReader;

import java.util.List;

public class ReaderServices {
    private ReaderDao rd;

    public ReaderDao getRd() {
        return rd;
    }

    public void setRd(ReaderDao rd) {
        this.rd = rd;
    }


    public boolean addTbReader(TbReader reader) {
        return rd.addTbReader(reader);
    }

    public boolean deleteTbReader(int rdId) {
        return rd.deleteTbReader(rdId);
    }

    public boolean updateTbReader(TbReader reader) {
        return rd.updateTbReader(reader);
    }

    public List queryByPage(int pageSize, int currentPage) {
        return rd.queryByPage(pageSize, currentPage);
    }

    public TbReader queryById(int rdId) {
        return rd.getTbReaderById(rdId);
    }

    public List queryByName(String rdName) {
        return rd.queryByName(rdName);
    }

    public List queryByType(String rdType) {
        return rd.queryByType(rdType);
    }

    public List queryByDept(String rdDept) {
        return rd.queryByDept(rdDept);
    }

    public int getCountAllNum() {
        return rd.countAllNum();
    }


}
