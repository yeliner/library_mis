package qyl.services;

import qyl.dao.BookDao;
import qyl.entity.TbBook;

import java.util.List;

public class BookServices {

    private BookDao bd;

    public BookDao getBd() {
        return bd;
    }

    public void setBd(BookDao bd) {
        this.bd = bd;
    }


    public boolean addTbBook(TbBook TbBook) {

        return bd.addTbBook(TbBook);
    }

    public boolean deleteTbBook(int TbBookId) {
        return bd.deleteTbBook(TbBookId);
    }

    public boolean updateTbBook(TbBook TbBook) {
        return bd.updateTbBook(TbBook);
    }

    public List queryByPage(int pageSize, int currentPage) {
        return bd.queryByPage(pageSize, currentPage);
    }

    public TbBook queryById(int bkId) {
        return bd.getTbBookById(bkId);
    }
    public List queryByName(String bkName) {
        return bd.queryByName(bkName);
    }
    public int getCountAllNum() {
        return bd.countAllNum();
    }

    public List queryByAuthor(String bkAuthor) {
        return bd.queryByAuthor(bkAuthor);
    }
}
