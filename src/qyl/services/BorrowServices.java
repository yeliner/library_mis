package qyl.services;

import qyl.dao.BorrowDao;
import qyl.entity.TbBorrow;

import java.util.List;

public class BorrowServices {
    private BorrowDao bd;


    public BorrowDao getBd() {
        return bd;
    }

    public void setBd(BorrowDao bd) {
        this.bd = bd;
    }

    public boolean addTbBorrow(TbBorrow borrow) {
        return bd.addTbBorrow(borrow);
    }

    public boolean deleteTbBorrow(int bdId) {
        return bd.deleteTbBorrow(bdId);
    }


    public List queryByPage(int pageSize, int currentPage) {
        return bd.queryByPage(pageSize, currentPage);
    }

    public TbBorrow queryById(int bdId) {
        return bd.getTbBorrowById(bdId);
    }

    public int getCountAllNum() {
        return bd.countAllNum();
    }

    public List queryByRdId(int id) {
        return bd.getTbBorrowByRdId(id);
    }

    public List queryByBkId(int id) {
        return bd.getTbBorrowByBkId(id);

    }

    public boolean updateLendBorrow(TbBorrow borrow) {
        return bd.updateLendBorrow(borrow);
    }

    public boolean updateContinueBorrow(TbBorrow borrow) {
        return bd.updateContinueBorrow(borrow);
    }
}
