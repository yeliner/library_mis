package qyl.action.reader;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReader;
import qyl.services.ReaderServices;
import qyl.services.ReaderTypeServices;

import java.io.IOException;
import java.util.Objects;

public class UpdateReaderAction extends BaseAction implements ModelDriven<TbReader> {
    private ReaderServices rs;
    private ReaderTypeServices rts;
    private TbReader reader = new TbReader();

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    @Override
    public TbReader getModel() {
        return reader;
    }
    private int rdTypeId;

    public int getRdTypeId() {
        return rdTypeId;
    }

    public void setRdTypeId(int rdTypeId) {
        this.rdTypeId = rdTypeId;
    }

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    public String updateReader() throws IOException {
        //id,rdName,rdSex,rdType,rdStatus,rdBorrowQty,rdPwd,rdAdminRoles不能为空
        if(rdTypeId==0|| Objects.equals(reader.getRdName(), "") ||
                reader.getRdSex().equals("")||reader.getRdPwd().equals("")||
                reader.getRdId()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "姓名/性别/读者类别/密码不能为空,reader修改失败!");
            System.out.println("信息为空修改失败");
            return "fail";
        }
        reader.setRdType(rts.getByRdType(rdTypeId));
        if (rs.updateTbReader(reader)) {
            System.out.println("Reader修改成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "Reader修改失败");
            System.out.println("Reader修改失败");
            return "fail";
        }
    }
    public String updateUser() throws IOException {
        if (rs.updateTbReader(reader)) {
            System.out.println("Reader修改成功");
            return "user-success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "Reader修改失败");
            System.out.println("Reader修改失败");
            return "user-fail";
        }
    }
}
