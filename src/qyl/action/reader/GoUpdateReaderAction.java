package qyl.action.reader;

import com.opensymphony.xwork2.ActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReader;
import qyl.services.ReaderServices;
import qyl.services.ReaderTypeServices;

import java.util.List;

public class GoUpdateReaderAction extends BaseAction {
    private ReaderServices rs;
    private int updateId;

    private ReaderTypeServices rts;

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    public int getUpdateId() {
        return updateId;
    }

    public void setUpdateId(int updateId) {
        this.updateId = updateId;
    }


    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    public String goUpdateReader() {
        TbReader Reader = rs.queryById(updateId);
        List list = rts.getAllReaderTypes();
        ActionContext.getContext().getSession().put("list", list);
        ActionContext.getContext().getSession().put("updateReader", Reader);
        return "toUpdateReader";
    }

    private int statusId;

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String goUpdateReaderStauts() {
        TbReader Reader = rs.queryById(statusId);
        List list = rts.getAllReaderTypes();
        ActionContext.getContext().getSession().put("list", list);
        ActionContext.getContext().getSession().put("statusReader", Reader);
        return "toUpdateReaderStauts";
    }

    private int roleId;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String goUpdateUserRole() {
        TbReader Reader = rs.queryById(roleId);
        List list = rts.getAllReaderTypes();
        ActionContext.getContext().getSession().put("list", list);
        ActionContext.getContext().getSession().put("roleUser", Reader);
        return "toUpdateUserRole";
    }

}
