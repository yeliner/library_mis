package qyl.action.reader;

import qyl.action.BaseAction;
import qyl.services.ReaderServices;

import java.io.IOException;

public class DeleteReaderAction extends BaseAction {
    private ReaderServices rs;

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    private int delRdId;

    public int getDelRdId() {
        return delRdId;
    }

    public void setDelRdId(int delRdId) {
        this.delRdId = delRdId;
    }

    public String deleteReader() throws IOException {
        if (rs.deleteTbReader(delRdId)) {
            System.out.println("Reader" + delRdId + "刪除成功！");
            return "success";
        } else {
            System.out.println("Reader" + delRdId + "刪除失败！");
            return "fail";
        }

    }

}
