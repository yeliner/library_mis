package qyl.action.reader;


import com.opensymphony.xwork2.ActionContext;
import qyl.entity.TbReaderType;
import qyl.services.ReaderTypeServices;

import java.util.List;

public class GoAddReaderAction {
    private ReaderTypeServices rts;

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    public String goAddReader() {
        List list = rts.getAllReaderTypes();
        ActionContext.getContext().getSession().put("list", list);
        return "AddReader";
    }

}
