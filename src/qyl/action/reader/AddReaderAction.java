package qyl.action.reader;

import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReader;
import qyl.entity.TbReaderType;
import qyl.services.ReaderServices;
import qyl.services.ReaderTypeServices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class AddReaderAction extends BaseAction implements ModelDriven<TbReader> {
    private ReaderServices rs;
    private ReaderTypeServices rts;
    private TbReader reader = new TbReader();

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    @Override
    public TbReader getModel() {
        return reader;
    }

    private int rdId;
    private int rdTypeId;

    public int getRdId() {
        return rdId;
    }

    public void setRdId(int rdId) {
        this.rdId = rdId;
    }

    public int getRdTypeId() {
        return rdTypeId;
    }

    public void setRdTypeId(int rdTypeId) {
        this.rdTypeId = rdTypeId;
    }

    public String addReader() throws IOException {
        //id,rdName,rdSex,rdType,rdStatus,rdBorrowQty,rdPwd,rdAdminRoles不能为空
        if(rdTypeId==0|| Objects.equals(reader.getRdName(), "") ||reader.getRdSex().equals("")||reader.getRdPwd().equals("")){
            ServletActionContext.getRequest().setAttribute("msgInfo", "姓名/性别/读者类别/密码不能为空,reader添加失败!");
            System.out.println("信息为空添加失败");
            return "fail";
        }
        //id自动生成
        reader.setRdStatus("有效");//读者状态默认为有效
        reader.setRdDateReg(new Date());//注册时间默认为当前时间
        reader.setRdAdminRoles(0);//默认为0
        reader.setRdBorrowQty(0);//默认借书为
        reader.setRdType(rts.getByRdType(rdTypeId));
        if (rs.addTbReader(reader)) {
            System.out.println("Reader添加成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo","Reader添加失败");
            System.out.println("Reader添加失败");
            return "fail";
        }
    }

}
