package qyl.action.reader;


import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReader;
import qyl.services.ReaderServices;
import qyl.util.PageUtil;

import java.util.List;

public class ShowUserAction extends BaseAction implements ModelDriven<TbReader> {
    private ReaderServices rs;
    private TbReader Reader = new TbReader();
    private String page;
    private String searchInfo;

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    @Override
    public TbReader getModel() {
        return Reader;
    }

    public String showUser() {
        PageUtil pageUtil = new PageUtil();

        pageUtil.setAllRecord(rs.getCountAllNum());

        pageUtil.count(this.getPage());
        List list = rs.queryByPage(pageUtil.getPageSize(),
                pageUtil.getCurrentPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        return "showUser";
    }


    public String getUsersById() {
        PageUtil pageUtil = new PageUtil();
        pageUtil.count(this.getPage());
        int id;
        try {
            id = Integer.parseInt(searchInfo.trim());
        } catch (NumberFormatException e) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "请输入用户编号(Number)!");
            ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
            System.out.println("格式转换错误");
            return "fail";
        }
        List list = null;
        list.add(rs.queryById(id));
        pageUtil.setAllRecord(list.size());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if (list.size() == 0) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该用户!");
        }
        return "getUsersById";
    }

    public String getUsersByName() {
        List list = rs.queryByName(searchInfo.trim());
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(list.size());
        pageUtil.count(this.getPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if (list.size() == 0) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该用户姓名!");
        }
        return "getUsersByName";
    }

    public String getUsersByType() {
        List list = rs.queryByType(searchInfo.trim());
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(list.size());
        pageUtil.count(this.getPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if (list.size() == 0) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该用户类别!");
        }
        return "getUsersByType";
    }

    public String getUsersByDept() {
        List list = rs.queryByDept(searchInfo.trim());
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(list.size());
        pageUtil.count(this.getPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if (list.size() == 0) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该用户系别!");
        }
        return "getUsersByDept";
    }
}
