package qyl.action;

import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class BaseAction {


    public void outText(String str) throws IOException {
        HttpServletResponse rep = ServletActionContext.getResponse();
        rep.setContentType("text/json;charset=utf-8");
        rep.setCharacterEncoding("utf-8");
        PrintWriter pw = rep.getWriter();
        pw.print(str);
        pw.flush();
        pw.close();
    }
}