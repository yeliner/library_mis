package qyl.action.book;

import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBook;
import qyl.services.BookServices;

import java.io.IOException;
import java.util.Objects;

public class UpdateBookAction extends BaseAction implements ModelDriven<TbBook> {
    private BookServices bs;
    private TbBook book = new TbBook();

    public BookServices getBs() {
        return bs;
    }

    public void setBs(BookServices bs) {
        this.bs = bs;
    }

    @Override
    public TbBook getModel() {
        return book;
    }

    public String updateBook() throws IOException {
        //id,图书编码,书名,作者,,ISBN,,状态不能为空
        if (Objects.equals(book.getBkAuthor(), "") || Objects.equals(book.getBkName(), "")
                || Objects.equals(book.getBkIsbn(), "") || book.getBkId() == 0
                || book.getBkCode() == null||book.getBkStatus()==null) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "作者/书名/Isbn/序号/编号/状态不能为空,Book修改失败!");
            System.out.println("信息为空,Book修改失败");
            return "fail";
        }
        if (bs.updateTbBook(book)) {
            System.out.println("Book修改成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "Book修改失败");
            System.out.println("Book修改失败");
            return "fail";
        }
    }

}
