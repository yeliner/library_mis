package qyl.action.book;

import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBook;
import qyl.services.BookServices;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

public class AddBookAction extends BaseAction implements ModelDriven<TbBook> {
    private BookServices bs;
    private TbBook book = new TbBook();

    public BookServices getBs() {
        return bs;
    }

    public void setBs(BookServices bs) {
        this.bs = bs;
    }

    @Override
    public TbBook getModel() {
        return book;
    }

    public String addBook() throws IOException {
        //作者,图书编码,ISBN,书名,状态不能为空
        //图书编码自动生成
        if (Objects.equals(book.getBkAuthor(), "") || Objects.equals(book.getBkName(), "") || Objects.equals(book.getBkIsbn(), "")) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "作者/书名/Isbn/不能为空,Book添加失败!");
            System.out.println("信息为空,Book添加失败");
            return "fail";
        }
        book.setBkBrief(book.getBkBrief().trim());//简介去空格
        book.setBkDateIn(new Date());//入库时为当前时间
        book.setBkStatus("在馆");//默认为在馆
        if (bs.addTbBook(book)) {
            System.out.println("Book添加成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "Book添加失败!");
            System.out.println("Book添加失败");
            return "fail";
        }
    }

}
