package qyl.action.book;

import com.opensymphony.xwork2.ActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBook;
import qyl.services.BookServices;

public class GoUpdateBookAction  extends BaseAction {
    private BookServices bs;
    private int bkId;
    public BookServices getBs() {
        return bs;
    }

    public void setBs(BookServices bs) {
        this.bs = bs;
    }
    public int getBkId() {
        return bkId;
    }

    public void setBkId(int bkId) {
        this.bkId = bkId;
    }
    public String goUpdateBook() {
        TbBook book = bs.queryById(bkId);
        ActionContext.getContext().getSession().put("book", book);
        return "toUpdateBook";
    }
}
