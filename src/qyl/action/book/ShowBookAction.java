package qyl.action.book;


import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBook;
import qyl.services.BookServices;
import qyl.util.PageUtil;

import java.util.List;

public class ShowBookAction extends BaseAction implements ModelDriven<TbBook> {
    private BookServices bs;
    private TbBook book=new TbBook();
    private String page;
    private String searchInfo;

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
    public BookServices getBs() {
        return bs;
    }

    public void setBs(BookServices bs) {
        this.bs = bs;
    }

    @Override
    public TbBook getModel() {
        return book;
    }
    public String showBook() {
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(bs.getCountAllNum());
        pageUtil.count(this.getPage());
        List list = bs.queryByPage(pageUtil.getPageSize(),
                pageUtil.getCurrentPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        return "success";
    }
    public String  getBooksByName(){
        List list=bs.queryByName(searchInfo.trim());
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(list.size());
        pageUtil.count(this.getPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if(list.size()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该书名!");
        }
        return "success";
    }
    public String  getBooksByAuthor(){
        List list=bs.queryByAuthor(searchInfo.trim());
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(list.size());
        pageUtil.count(this.getPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if(list.size()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该作者!");
        }
        return "success";
    }

}
