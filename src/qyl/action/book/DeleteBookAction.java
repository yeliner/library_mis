package qyl.action.book;

import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.services.BookServices;

import java.io.IOException;

public class DeleteBookAction extends BaseAction {
    private BookServices bs;

    public BookServices getBs() {
        return bs;
    }

    public void setBs(BookServices bs) {
        this.bs = bs;
    }

    private int delBkId;

    public int getDelBkId() {
        return delBkId;
    }

    public void setDelBkId(int delBkId) {
        this.delBkId = delBkId;
    }

    public String deleteBook() throws IOException {
        if (bs.deleteTbBook(delBkId)) {
            System.out.println("Book" + delBkId + "刪除成功！");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo","Book" + delBkId + "刪除失败！");
            System.out.println("Book" + delBkId + "刪除失败！");
            return "fail";
        }

    }

}
