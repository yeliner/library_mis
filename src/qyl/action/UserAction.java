package qyl.action;

import com.opensymphony.xwork2.ActionContext;
import qyl.entity.TbReader;
import qyl.services.ReaderServices;

import java.io.IOException;

public class UserAction extends BaseAction {
    private ReaderServices rs;

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    private String newRdPwd;

    public String getNewRdPwd() {
        return newRdPwd;
    }

    public void setNewRdPwd(String newRdPwd) {
        this.newRdPwd = newRdPwd;
    }

    public void updateUserPwd() throws IOException {

        if (newRdPwd == null) {
            System.out.println("新密码为空修改失败");
            this.outText("新密码为空修改失败");
            return;
        }
        TbReader user = ((TbReader) ActionContext.getContext().getSession()
                .get("reader"));
        if (user == null) {
            System.out.println("登陆用户获取失败!");
            this.outText("登陆用户获取失败");
            return;

        }
        user.setRdPwd(newRdPwd);
        if (rs.updateTbReader(user)) {
            ActionContext.getContext().getSession().replace("reader", user);
            this.outText("success");
            System.out.println("success");
        } else {
            this.outText("密码修改失败");
            System.out.println("密码修改失败");
        }
    }
}
