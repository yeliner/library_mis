package qyl.action;

import com.opensymphony.xwork2.ActionContext;
import org.apache.struts2.ServletActionContext;
import qyl.entity.TbReader;
import qyl.services.ReaderServices;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class LoginAction extends BaseAction {
    private ReaderServices rs;

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }


    public void login() throws IOException {
        HttpServletRequest request = ServletActionContext.getRequest();
        String str = "";
        int rdId;
        try {
            rdId = Integer.parseInt(request.getParameter("rdId"));
        } catch (NumberFormatException e) {
            str = "请输入用户编号(Number)";
            this.outText(str);
            System.out.println("格式转换错误");
            return;
        }
        String rdPwd = request.getParameter("rdPwd");
        TbReader r = rs.queryById(rdId);
        if (r != null) {
            if (r.getRdPwd().equals(rdPwd)) {
                ActionContext.getContext().getSession().put("reader", r);
                str="success";
            } else {
                str = "密码输入错误";
            }
        } else {
            str = "读者不存在";
        }
        this.outText(str);
        System.out.println(str);
    }

    public void getSessionUser() throws IOException {
        TbReader reader = ((TbReader) ActionContext.getContext().getSession()
                .get("reader"));
        if (reader != null) {
            String rdName = reader.getRdName();
            this.outText(rdName);
        } else {
            this.outText("nologin");
        }
    }


}

