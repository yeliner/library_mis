package qyl.action.borrow;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBook;
import qyl.entity.TbBorrow;
import qyl.entity.TbReader;
import qyl.services.BookServices;
import qyl.services.BorrowServices;
import qyl.services.ReaderServices;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class AddBorrowAction implements ModelDriven<TbBorrow> {
    private BorrowServices bs;
    private BookServices bookServices;
    private ReaderServices readerServices;
    private TbBorrow borrow = new TbBorrow();
    private String page;
    private String searchInfo;

    public ReaderServices getReaderServices() {
        return readerServices;
    }

    public void setReaderServices(ReaderServices readerServices) {
        this.readerServices = readerServices;
    }

    public BookServices getBookServices() {
        return bookServices;
    }

    public void setBookServices(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    public BorrowServices getBs() {
        return bs;
    }

    public void setBs(BorrowServices bs) {
        this.bs = bs;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    @Override
    public TbBorrow getModel() {
        return borrow;
    }

    public String getReaderById() {
        int id;
        try {
            id = Integer.parseInt(searchInfo.trim());
        } catch (NumberFormatException e) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "请输入借书号(Number)!");
            System.out.println("格式转换错误");
            return "fail";
        }
        TbReader borrowReader = readerServices.queryById(id);
        ActionContext.getContext().getSession().put("borrowReader", borrowReader);
        return "getReaderById";
    }

    public String getBookById() {
        int id;
        try{
            id = Integer.parseInt(searchInfo.trim());
        } catch (NumberFormatException e) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "请输入图书证号(Number)!");
            System.out.println("格式转换错误");
            return "fail";
        }
        TbBook borrowBook = bookServices.queryById(id);
        ActionContext.getContext().getSession().put("borrowBook", borrowBook);
        return "getBookById";
    }

    public String addBorrow() {
        //借书证号,读者,图书,续借次数,借书时间,应还时间,是否已还书,借书操作员不能为空
        TbReader borrowReader = ((TbReader) ActionContext.getContext().getSession()
                .get("borrowReader"));
        TbBook borrowBook = ((TbBook) ActionContext.getContext().getSession()
                .get("borrowBook"));
        TbReader loginUser = ((TbReader) ActionContext.getContext().getSession()
                .get("reader"));
        if (borrowReader == null || borrowBook == null || loginUser == null) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "borrowReader/borrowBook/loginUser获取失败!");
            System.out.println("信息为空添加失败");
            return "add_fail";
        }
        if(!Objects.equals(borrowReader.getRdStatus(), "有效")){
            ServletActionContext.getRequest().setAttribute("msgInfo", "该借书证"+borrowReader.getRdStatus()+",无法借书!");
            System.out.println("证件无效");
            return "add_fail";
        }
        if(borrowReader.getRdBorrowQty()==borrowReader.getRdType().getCanLendQty()){
            ServletActionContext.getRequest().setAttribute("msgInfo", "该借书证借书数量已达上限!无法借书!");
            System.out.println("已达上限");
            return "add_fail";
        }
        if(!Objects.equals(borrowBook.getBkStatus(), "在馆")){
            ServletActionContext.getRequest().setAttribute("msgInfo", "该图书不在馆!无法借书!");
            System.out.println("不在馆");
            return "add_fail";
        }
        borrow.setTbReaderByRdId(borrowReader);
        borrow.setTbBookByBkId(borrowBook);
        borrow.setOperatorRet(loginUser.getRdName());//借书操作员未当前登陆用户
        borrow.setLdContinueTimes(0);//续借次数为0
        borrow.setLdDateOut(new Date());//借书时间为当前时间
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, borrowReader.getRdType().getCanLendDay());
        borrow.setLdDateRetPlan(c.getTime());//应还时间为读者最多借书时间
        borrow.setLsHasReturn(false);//借书即未还
        if (bs.addTbBorrow(borrow)) {
            System.out.println("Borrow添加成功");
            return "add_success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "借书失败!");
            System.out.println("Borrow添加失败");
            return "add_fail";
        }
    }


}
