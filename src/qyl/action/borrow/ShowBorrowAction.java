package qyl.action.borrow;

import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBorrow;
import qyl.services.BorrowServices;
import qyl.util.PageUtil;

import java.util.List;

public class ShowBorrowAction extends BaseAction implements ModelDriven<TbBorrow> {
    private BorrowServices bs;
    private TbBorrow borrow = new TbBorrow();
    private String page;
    private String searchInfo;


    public BorrowServices getBs() {
        return bs;
    }

    public void setBs(BorrowServices bs) {
        this.bs = bs;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    @Override
    public TbBorrow getModel() {
        return borrow;
    }

    public String showBorrow() {
        PageUtil pageUtil = new PageUtil();

        pageUtil.setAllRecord(bs.getCountAllNum());

        pageUtil.count(this.getPage());
        List list = bs.queryByPage(pageUtil.getPageSize(),
                pageUtil.getCurrentPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        return "showBorrow";
    }

    public String getBorrowsByRdId() {
        PageUtil pageUtil = new PageUtil();
        pageUtil.count(this.getPage());
        int id;
        try {
            id = Integer.parseInt(searchInfo.trim());
        } catch (NumberFormatException e) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "请输入借书证号(Number)!");
            ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
            System.out.println("格式转换错误");
            return "fail";
        }
        List list = bs.queryByRdId(id);
        pageUtil.setAllRecord(list.size());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if(list.size()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该借书证号!");
        }
        return "getBorrowsByRdId";
    }

    public String getBorrowsByBkId() {
        PageUtil pageUtil = new PageUtil();
        pageUtil.count(this.getPage());
        int id;
        try {
            id = Integer.parseInt(searchInfo.trim());
        } catch (NumberFormatException e) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "请输入图书序号(Number)!");
            ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
            System.out.println("格式转换错误");
            return "fail";
        }
        List list = bs.queryByRdId(id);
        pageUtil.setAllRecord(list.size());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if(list.size()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该图书序号!");
        }
        return "getBorrowsByBkId";
    }


}