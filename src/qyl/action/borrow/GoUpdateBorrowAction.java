package qyl.action.borrow;

import com.opensymphony.xwork2.ActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBorrow;
import qyl.services.BorrowServices;

import java.util.Date;

public class GoUpdateBorrowAction extends BaseAction {
    private BorrowServices bs;
    private int lendBorrowId;

    public BorrowServices getBs() {
        return bs;
    }

    public void setBs(BorrowServices bs) {
        this.bs = bs;
    }

    public int getLendBorrowId() {
        return lendBorrowId;
    }

    public void setLendBorrowId(int lendBorrowId) {
        this.lendBorrowId = lendBorrowId;
    }

    public String goLendBorrow() {
        TbBorrow borrow = bs.queryById(lendBorrowId);
        borrow.setLdDateRetAct(new Date());//当前时间为还书时间
        Date actDate = borrow.getLdDateRetAct();
        Date planDate = borrow.getLdDateRetPlan();
        if (actDate.after(planDate)) {
            //actDate>planDate,即逾期
            //逾期天数
            int day = (int) ((actDate.getTime() - planDate.getTime()) / (1000 * 3600 * 24));
            //逾期罚款,应罚款金额=超期天数*罚款率
            double money = day * borrow.getTbReaderByRdId().getRdType().getPunishRate();
            borrow.setLdOverDay(day);
            borrow.setLdOverMoney(money);
        }
        ActionContext.getContext().getSession().put("lendBorrow", borrow);
        return "toLendBorrow";
    }

}
