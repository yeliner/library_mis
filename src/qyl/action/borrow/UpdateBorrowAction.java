package qyl.action.borrow;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.entity.TbBorrow;
import qyl.entity.TbReader;
import qyl.services.BookServices;
import qyl.services.BorrowServices;
import qyl.services.ReaderServices;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class UpdateBorrowAction implements ModelDriven<TbBorrow> {
    private BorrowServices bs;
    private TbBorrow borrow = new TbBorrow();
    private BookServices bookServices;
    private ReaderServices readerServices;

    public BorrowServices getBs() {
        return bs;
    }

    public void setBs(BorrowServices bs) {
        this.bs = bs;
    }

    public BookServices getBookServices() {
        return bookServices;
    }

    public void setBookServices(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    public ReaderServices getReaderServices() {
        return readerServices;
    }

    public void setReaderServices(ReaderServices readerServices) {
        this.readerServices = readerServices;
    }

    @Override
    public TbBorrow getModel() {
        return borrow;
    }
    private int lendRdId;
    private int lendBkId;

    public int getLendRdId() {
        return lendRdId;
    }

    public void setLendRdId(int lendRdId) {
        this.lendRdId = lendRdId;
    }

    public int getLendBkId() {
        return lendBkId;
    }

    public void setLendBkId(int lendBkId) {
        this.lendBkId = lendBkId;
    }

    public String lendBorrow() {
        //借书证号,读者,图书,续借次数,借书时间,应还时间,是否已还书,借书操作员不能为空
        TbReader loginUser = ((TbReader) ActionContext.getContext().getSession()
                .get("reader"));
        if (borrow == null||lendBkId==0||lendRdId==0||loginUser==null) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "信息获取失败,还书失败!");
            System.out.println("信息为空还书失败");
            return "fail";
        }
        borrow.setTbReaderByRdId(readerServices.queryById(lendRdId));
        borrow.setTbBookByBkId(bookServices.queryById(lendBkId));
        borrow.setOperatorLend(loginUser.getRdName());
        borrow.setLdDateRetAct(new Date());
        borrow.setLsHasReturn(true);
        if (bs.updateLendBorrow(borrow)) {
            System.out.println("borrow还书成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "borrow还书失败");
            System.out.println("borrow还书失败");
            return "fail";
        }
    }


    private int continueBorrowId;

    public int getContinueBorrowId() {
        return continueBorrowId;
    }

    public void setContinueBorrowId(int continueBorrowId) {
        this.continueBorrowId = continueBorrowId;
    }

    public String continueBorrow() {
        TbBorrow borrow = bs.queryById(continueBorrowId);
        if (!Objects.equals(borrow.getTbReaderByRdId().getRdStatus(), "有效")) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "该借书证" + borrow.getTbReaderByRdId().getRdStatus() + ",无法续借!");
            System.out.println("证件无效");
            return "continue_fail";
        }
        if (Objects.equals(borrow.getTbReaderByRdId().getRdType().getCanContinueTimes(), borrow.getLdContinueTimes())) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "该借书证续借数量已达上限!无法续借!");
            System.out.println("已达上限");
            return "continue_fail";
        }
        if (Objects.equals(borrow.getTbBookByBkId().getBkStatus(), "在馆")) {
            ServletActionContext.getRequest().setAttribute("msgInfo", "该图在馆!无法续借!");
            System.out.println("在馆");
            return "continue_fail";
        }

        Calendar c = Calendar.getInstance();
        c.setTime(borrow.getLdDateRetPlan());
        c.add(Calendar.DATE, borrow.getTbReaderByRdId().getRdType().getCanLendDay());
        borrow.setLdDateRetPlan(c.getTime());//应还日期延长
        borrow.setLdContinueTimes(borrow.getLdContinueTimes()+1);//续借次数加1
        if (bs.updateContinueBorrow(borrow)) {
            System.out.println("borrow续借成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "borrow续借失败");
            System.out.println("borrow续借失败");
            return "continue_fail";
        }
    }

}
