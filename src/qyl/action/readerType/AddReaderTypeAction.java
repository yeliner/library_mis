package qyl.action.readerType;

import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReaderType;
import qyl.services.ReaderTypeServices;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

public class AddReaderTypeAction extends BaseAction implements ModelDriven<TbReaderType> {
    private ReaderTypeServices rts;
    private TbReaderType readerType = new TbReaderType();

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    @Override
    public TbReaderType getModel() {
        return readerType;
    }
    public String addReaderType() throws IOException {
        //rdType.Name不能重复,属性都不能为空,

        if(Objects.equals(readerType.getRdTypeName(), "") ||readerType.getCanLendQty()==null||
                readerType.getCanLendDay()==null||readerType.getCanContinueTimes()==null||
                readerType.getPunishRate()==null){
            ServletActionContext.getRequest().setAttribute("msgInfo", "信息不能为空!");
            System.out.println("信息为空,readerType添加失败");
            return "fail";
        }
        if(rts.queryByName1(readerType.getRdTypeName())!=null){
            ServletActionContext.getRequest().setAttribute("msgInfo", "该类别名已存在!");
            System.out.println("类别名不能重复");
            return "fail";

        }
        if (rts.addTbReaderType(readerType)) {
            System.out.println("readerType添加成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "readerType添加失败!");
            System.out.println("readerType添加失败");
            return "fail";
        }
    }

}
