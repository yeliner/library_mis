package qyl.action.readerType;

import com.opensymphony.xwork2.ActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbBook;
import qyl.entity.TbReaderType;
import qyl.services.BookServices;
import qyl.services.ReaderTypeServices;

public class GoUpdateReaderTypeAction  extends BaseAction {
    private ReaderTypeServices rts;
    private int rdType;

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    public int getRdType() {
        return rdType;
    }

    public void setRdType(int rdType) {
        this.rdType = rdType;
    }

    public String goUpdateReaderType() {
        TbReaderType readerType = rts.queryByRdType(rdType);
        ActionContext.getContext().getSession().put("readerType", readerType);
        return "toUpdateReaderType";
    }
}
