package qyl.action.readerType;

import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReaderType;
import qyl.entity.TbReaderType;
import qyl.services.ReaderTypeServices;

import java.io.IOException;
import java.util.Objects;

public class UpdateReaderTypeAction extends BaseAction implements ModelDriven<TbReaderType> {
    private ReaderTypeServices rts;
    private TbReaderType readerType = new TbReaderType();

    @Override
    public TbReaderType getModel() {
        return readerType;
    }

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    public String updateReaderType() throws IOException {
        if(Objects.equals(readerType.getRdTypeName(), "") ||readerType.getCanLendQty()==0||
                readerType.getCanLendDay()==0||readerType.getCanContinueTimes()==0||
                readerType.getPunishRate()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "信息不能为空!");
            System.out.println("信息为空,readerType添加失败");
            return "fail";
        }
      /*  if(rts.queryByName(readerType.getRdTypeName())!=null){
            ServletActionContext.getRequest().setAttribute("msgInfo", "该类别名已存在!");
            System.out.println("类别名不能重复");
            return "fail";

        }*/
        if (rts.updateTbReaderType(readerType)) {
            System.out.println("ReaderType修改成功");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "ReaderType修改失败");
            System.out.println("ReaderType修改失败");
            return "fail";
        }
    }

}
