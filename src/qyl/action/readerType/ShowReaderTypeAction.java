package qyl.action.readerType;


import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReaderType;
import qyl.services.ReaderTypeServices;
import qyl.util.PageUtil;

import java.util.List;

public class ShowReaderTypeAction extends BaseAction implements ModelDriven<TbReaderType> {
    private ReaderTypeServices rts;
    private TbReaderType ReaderType=new TbReaderType();
    private String page;
    private String searchInfo;

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    @Override
    public TbReaderType getModel() {
        return ReaderType;
    }
    public String showReaderType() {
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(rts.getCountAllNum());
        pageUtil.count(this.getPage());
        List list = rts.queryByPage(pageUtil.getPageSize(),
                pageUtil.getCurrentPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        return "success";
    }
    public String  getReaderTypesByName(){
        List list=rts.queryByName(searchInfo.trim());
        PageUtil pageUtil = new PageUtil();
        pageUtil.setAllRecord(list.size());
        pageUtil.count(this.getPage());
        pageUtil.setCurrentList(list);
        ServletActionContext.getRequest().setAttribute("pageUtil", pageUtil);
        if(list.size()==0){
            ServletActionContext.getRequest().setAttribute("msgInfo", "无该类名!");
        }
        return "success";
    }

}
