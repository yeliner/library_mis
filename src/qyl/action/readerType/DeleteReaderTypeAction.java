package qyl.action.readerType;

import org.apache.struts2.ServletActionContext;
import qyl.action.BaseAction;
import qyl.entity.TbReaderType;
import qyl.services.ReaderServices;
import qyl.services.ReaderTypeServices;

import java.io.IOException;

public class DeleteReaderTypeAction extends BaseAction {
    private ReaderTypeServices rts;
    private ReaderServices rs;

    private int delRdType;

    public ReaderTypeServices getRts() {
        return rts;
    }

    public void setRts(ReaderTypeServices rts) {
        this.rts = rts;
    }

    public ReaderServices getRs() {
        return rs;
    }

    public void setRs(ReaderServices rs) {
        this.rs = rs;
    }

    public int getDelRdType() {
        return delRdType;
    }

    public void setDelRdType(int delRdType) {
        this.delRdType = delRdType;
    }

    public String deleteReaderType() throws IOException {
        TbReaderType tbReaderType=rts.getByRdType(getDelRdType());
      /*  if (rs.queryByType(tbReaderType.getRdTypeName())!=null){
            ServletActionContext.getRequest().setAttribute("msgInfo", "该类别正在使用,不能删除!");
            System.out.println("该类别正在使用,不能删除!");
            return "fail";
        }*/
        if (rts.deleteTbReaderType(delRdType)) {
            System.out.println("ReaderType" + delRdType + "刪除成功！");
            return "success";
        } else {
            ServletActionContext.getRequest().setAttribute("msgInfo", "ReaderType" + delRdType + "刪除失败！");
            System.out.println("ReaderType" + delRdType + "刪除失败！");
            return "fail";
        }

    }

}
